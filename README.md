<img src="https://gitlab.com/senac-tcc-2019/ogroburger/raw/master/outros/imagens/2019-03-08_-_PARA-USO-GERAL-NO-PROJETO-DO-GITLAB.png" width="180">

#  Ogro Burger

O "Ogro Burger" é um sistema *web* 
(**[Dashboard](https://pt.wikipedia.org/wiki/Painel_de_bordo)**) para 
administradores e basicamente um **[SPA](https://pt.wikipedia.org/wiki/Aplicativo_de_p%C3%A1gina_%C3%BAnica)** 
para os clientes, completamente codificado em 
**[Laravel](https://laravel.com/)** 
(*[framework PHP](https://pt.wikipedia.org/wiki/Framework_para_aplica%C3%A7%C3%B5es_web)*) 
que contará com algumas outras tecnologias auxiliares para o seu 
desenvolvimento: 
**[jQuery](https://jquery.com/)**, **[Bootstrap 4](https://getbootstrap.com/)**, 
**[MySQL](https://www.mysql.com/)** e 
[outras](https://gitlab.com/senac-tcc-2019/ogroburger/wikis/home)... 
Embora o projeto esteja na fase inicial, ele oferece uma visão ampla de uma 
aplicação simples usando essas tecnologias e em breve o mesmo será colocado em 
produção atendendo as expectativas de um cliente, pois o mesmo não se trata 
somente de um sistema fictício como requisito da disciplina de 
Projeto de Desenvolvimento (TCC I) do 5° semestre. O projeto (sistema *web* 
e SPA) basicamente consiste em possibilitar o administrador controlar o cadastro 
de novos usuários (clientes) no sistema, a venda de "hamburguês", ou seja, 
pedidos realizados pelos usuários (clientes) e todo o controle de estoque... 
Para ter mais detalhes da dinâmica do negócio clique 
[aqui](https://gitlab.com/senac-tcc-2019/ogroburger/wikis/home).

O projeto “Ogro Burger” começou a ser desenvolvido na aula 01 (atividade única) 
e faz parte da avaliação da disciplina de Projeto de Desenvolvimento (TCC I) do 
5° semestre da 
**[Faculdade de Tecnologia Senac Pelotas](https://www.senacrs.com.br/unidades.asp?unidade=78)** 
(RS) e é mantido hoje por mim (não mais por um grupo que antes pertencia a outra 
disciplina — [projeto/repositório](https://gitlab.com/angelogl/ogroburger1)): 
**[Rafael Calearo](https://gitlab.com/rafaelcalearo)**. O projeto conta também 
com a orientação do prof. **[Angelo Luz](https://gitlab.com/angelogl)** e o 
andamento do mesmo pode ser acompanhado (visto) no repositório do 
**[Heroku](https://www.heroku.com/)** (http://ogroburger.herokuapp.com/) ou em produção (*online*)!
O acesso a área administrativa pode ser acessada por aqui: 
http://ogroburger.herokuapp.com/admin/login (usuário: rafaelcalearo@hotmail.com e senha: 123456).

🚧 **Situação atual do projeto:** em reconstrução/desenvolvimento!

Sinta-se livre para clonar ou baixar o código-fonte do projeto

## Pré-requisitos

O Laravel assim como outros 
*[frameworks](https://pt.wikipedia.org/wiki/Framework)* necessitam de alguns 
[requisitos de sistema](https://laravel.com/docs/5.6/installation#installation). 
O site do Laravel sugere que para desenvolver aplicações em Laravel o ideal é 
usar a máquina virtual 
**[Laravel Homestead](https://laravel.com/docs/5.6/homestead)**, pois ela atende 
a todos esses 
[requisitos de sistema](https://laravel.com/docs/5.6/installation#installation). 
Porém, caso você não use o Homestead o recomendado seria o uso da ferramenta 
**[XAMPP](https://www.apachefriends.org/pt_br/index.html)** ou similar, pois é 
fácil de instalar e possui todo o ambiente de desenvolvimento necessário para 
desenvolver aplicações em Laravel ou "rodar" aplicações baixadas (prontas) ou em 
desenvolvimento como essa!

Outra ferramenta que faz parte dos requisitos de sistema é o 
**[Composer](https://getcomposer.org/)** que serve para gerenciar dependências. 
Nesse "README" não será mostrado como instalar essas ferramentas então, fique a 
vontade em clicar nos *links* fornecidos anteriormente para obter a documentação 
dos mesmos junto aos seus sites oficiais ou assista esse 
[vídeo](https://www.youtube.com/watch?v=V1RA7V2Kn8g&lc=z22pxbehrszbc1xag04t1aokgbxwjxqj4o1po2swbyl2rk0h00410) 
no YouTube.

⚠ ***ATENÇÃO, as ferramentas, comandos, linguagens (*framework*) e etc. requerem conhecimentos prévios!***

## Índice

* [Clonar](#clonar)
* [How to (como usar)](#uso)
* [O grupo](#grupo)
* [Documentação](#documentação)
* [Licença](#licença)

## Clonar

Para efetuar o clone do projeto usando os *links* disponíveis no repositório do 
GitLab é preciso possuir o *software* Git instalado na sua máquina. Para saber 
como instalar e usar a ferramenta Git acesse o site do Git: 
<https://git-scm.com/doc>. Caso já saiba como fazer use os comandos abaixo:

**Com SSH:**

```bash
$ git clone git@gitlab.com:senac-tcc-2019/ogroburger.git
```

ou 

**Com HTTPS:**

```bash
$ git clone https://gitlab.com/senac-tcc-2019/ogroburger.git
```

ou você pode baixar o projeto (*download*): 
[zip](https://gitlab.com/senac-tcc-2019/ogroburger/-/archive/master/ogroburger-master.zip),
[tar.gz](https://gitlab.com/senac-tcc-2019/ogroburger/-/archive/master/ogroburger-master.tar.gz),
[tar.bz2](https://gitlab.com/senac-tcc-2019/ogroburger/-/archive/master/ogroburger-master.tar.bz2) e 
[tar](https://gitlab.com/senac-tcc-2019/ogroburger/-/archive/master/ogroburger-master.tar).

## Uso

**ANTES DE USAR:** leia os [pré-requisitos](#pré-requisitos).

Após ter clonado/baixado o projeto, execute os seguintes comandos listados 
abaixo no diretório do projeto usando as ferramentas de sua preferência 
([Prompt de Comando — cmd.exe — Windows](https://pt.wikipedia.org/wiki/Cmd.exe), 
[terminal do Linux](https://pt.wikipedia.org/wiki/Bash), Git ou 
[IDE](https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado)):

* `composer install`: serve para ler o arquivo `composer.lock` e instalar as dependências listadas nesse arquivo;
* `composer dump`: serve para recarregar a lista de **classes**, **pacotes** e **bibliotecas** que estão dentro do seu projeto no arquivo de **autoload**;
* `cp .env.example .env`: serve para criar o arquivo `.env` caso o projeto não possua (Linux);
* `copy .env.example .env`: serve para criar o arquivo `.env` caso o projeto não possua (Windows);
* `php artisan key:generate`: serve para gerar uma chave do tipo de segurança para o seu arquivo `.env`;
* `php artisan migrate`: serve para criar as tabelas (arquivos) compostos no projeto no **banco de dados** que estará rodando no momento;
* `php artisan db:seed`: serve para **inserir** registros nas tabelas criadas anteriormente;
* `php artisan serve`: e por fim, esse comando serve para iniciar a aplicação que pode ser visualizada no **navegador** (*browser*).

⚠ ***ATENÇÃO, É PRECISO CONFIGURAR O ARQ. .ENV APÓS CRIADO CONFORME O SEU BD, EM CASO DE DÚVIDAS ACESSE O [VÍDEO](https://www.youtube.com/watch?v=V1RA7V2Kn8g&lc=z22pxbehrszbc1xag04t1aokgbxwjxqj4o1po2swbyl2rk0h00410)!***

## Grupo

Como mencionado anteriormente, o projeto era desenvolvido em outra disciplina e 
essa exigia que o mesmo fosse feito em grupo, porem, hoje como também foi 
mencionado lá encima, o Ogro Burger é desenvolvido por mim:

* **[Rafael Calearo](https://gitlab.com/rafaelcalearo)**;
* **~~[Leandro Santos](https://gitlab.com/leandro2206)~~**;
* **~~[Yuri Ferreira](https://gitlab.com/YuriFerreira)~~**;
* **~~[Sérgio Filho](https://gitlab.com/SergioDias)~~**;
* **~~[Caiã Ceron](https://gitlab.com/caiaceron)~~**.

## Documentação

A documentação com maior clareza se encontra nas seguintes formas:

* **[Wiki](https://gitlab.com/senac-tcc-2019/ogroburger/wikis/home)** desse repositório;
* **[Artigo](https://www.overleaf.com/read/tqhdrknzmmdv)** disponibilizado pelo [Overleaf](https://www.overleaf.com/).

## Licença

A aplicação conta com a seguinte licença de uso: 
**[MIT](https://opensource.org/licenses/MIT).**
