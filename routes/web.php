<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

/*
|--------------------------------------------------------------------------
| COMANDO: php artisan make:auth QUE CRIA ESSAS LINHAS ABAIXO! 
|--------------------------------------------------------------------------
*/
//AUTH - CRIA UMAS QUANTAS ROTAS OCULTAS, APENAS C/ ESSA LINHA LOGO ABAIXO:
Auth::routes();
//CONTINUACAO DO AUTH, ROTAS CRIADAS P/ GERENCIAR A PARTE DE LOGIN DE USUARIOS
Route::get('/home', 'HomeController@index')->name('home');
//ROTAS P/ EDICAO DO PERFIL DO USUARIO LOGADO
Route::get('/home/perfil/{id}', 'HomeController@perfil')->name('home.perfil');
Route::post('/home/{id}', 'HomeController@update');
//CONTINUACAO DO AUTH, ROTAS CRIADAS P/ GERENCIAR A PARTE DE LOGIN DE ADMINS
Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
Route::get('/admin/login', 'Auth\AdminLoginController@index')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
//ROTAS P/ EDICAO DO PERFIL DO ADMIN LOGADO
Route::get('/admin/perfil/{id}', 'AdminController@perfil')->name('admin.perfil');
Route::post('/admin/{id}', 'AdminController@update');



//ROTAS P/ CLIENTES QUE FUNCIONAM DENTRO DO DASHBOARD
Route::get('/admin/clientes', 'ClienteController@index');
Route::get('/admin/clientes/az', 'ClienteController@az');
Route::get('/admin/clientes/za', 'ClienteController@za');
Route::get('/admin/clientes/desativados', 'ClienteController@desativados');
Route::get('/admin/clientes/controle', 'ClienteController@controleClientes');
Route::get('/admin/clientes/desativar/{id}', 'ClienteController@desativar');
Route::get('/admin/clientes/ativar/{id}', 'ClienteController@ativar');
Route::get('/admin/clientes/editar/{id}', 'ClienteController@edit');
Route::post('/admin/clientes/editar/{id}', 'ClienteController@update');
Route::get('/admin/cliente/novo', 'ClienteController@create');
Route::post('/admin/cliente/novo', 'ClienteController@store');
Route::post('/admin/clientes/pesquisar', 'ClienteController@pesquisar');
Route::get('/admin/clientes/procurar', 'ClienteController@procurar');
Route::get('/admin/clientes/pdf-clientes-ativos', 'ClienteController@pdfClientesAtivos');
Route::get('/admin/clientes/pdf-clientes-desativados', 'ClienteController@pdfClientesDesativados');
Route::get('/admin/clientes/pdf-clientes-ativos-download', 'ClienteController@pdfClientesAtivosDownload');
Route::get('/admin/clientes/pdf-clientes-desativados-download', 'ClienteController@pdfClientesDesativadosDownload');
Route::get('/admin/clientes/perfil', 'ClienteController@perfil');

Route::get('/admin/pedidos', 'AdminController@pedidos');
Route::get('/admin/novo-pedido', 'AdminController@novoPedido');
Route::get('/admin/pedido', 'AdminController@pedido');
Route::get('/admin/pedido/encerrar/{id}', 'AdminController@encerrar');
Route::post('/admin/pedido/editar/{id}', 'AdminController@editarPedido');
Route::post('/admin/pedido/new', 'AdminController@newPedido');

//ROTAS P/ LANCHES QUE FUNCIONAM DENTRO DO DASHBOARD
Route::get('/admin/lanches', 'LancheController@index');
Route::get('/admin/pedido/lanches', 'AdminController@lanchesPedido');
Route::get('/admin/lanche', 'AdminController@lanche');
Route::get('/admin/lanche/procurar', 'AdminController@procurar');
Route::get('/admin/lanches/az', 'LancheController@az');

//ROTAS P/ FORNECEDORES QUE FUNCIONAM DENTRO DO DASHBOARD
Route::get('/admin/fornecedores', 'MercadoController@index');
Route::get('/admin/fornecedores/desativados', 'MercadoController@desativados');
Route::get('/admin/fornecedores/controles', 'MercadoController@controles');
Route::get('/admin/fornecedores/desativar/{id}', 'MercadoController@desativar');
Route::get('/admin/fornecedores/ativar/{id}', 'MercadoController@ativar');
Route::post('/admin/fornecedores/busca', 'MercadoController@busca');
Route::get('/admin/fornecedores/procurar', 'MercadoController@procurar');

//ROTAS P/ PRODUTOS QUE FUNCIONAM DENTRO DO DASHBOARD
Route::get('/admin/produtos', 'ProdutoController@index');

//ROTAS P/ COMPRAS QUE FUNCIONAM DENTRO DO DASHBOARD
Route::get('/admin/compras', 'CompraController@index');
Route::post('/admin/compras/nova', 'CompraController@store');