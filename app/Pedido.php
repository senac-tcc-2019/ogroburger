<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    function lanches() {
        return $this->belongsToMany("App\Lanche", "carrinhos");
    }
    function usuario() {
        return $this->belongsToMany("App\User", "carrinhos");
    }
}
