<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lanche extends Model
{
    function produtos() {
        return $this->belongsToMany("App\Produto", "recheios")->withPivot('quantidadeproduto');
    }
}
