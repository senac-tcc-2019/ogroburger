<?php

namespace App\Http\Controllers;

use App\Mercado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MercadoController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fornecedores = Mercado::where('ativo', 0)->orderBy('razaosocial')->paginate(5);
        if ($request->ajax()) {
            return view('componentes.tabela-fornecedores', array('fornecedores' => $fornecedores))->render();
        }
        return view('fornecedores', compact('fornecedores'));       
    }

    public function desativados()
    {
        $fornecedores = Mercado::where('ativo', 1)->orderBy('razaosocial')->get();
        return json_encode($fornecedores);
    }

    public function controles()
    {
        $controles['totalFornecedores'] = Mercado::all()->count();        
        $controles['dataUltimo'] = date("d/m/Y", strtotime(Mercado::max('created_at')));
        $controles['totalDesativados'] = Mercado::where('ativo', 1)->count();      
        $controles['dataDesativados'] = date("d/m/Y", strtotime(Mercado::where('ativo', 1)->max('updated_at')));
        $controles['semTelefone'] = Mercado::where('telefone', "—")->count();
        $controles['dataTelefone'] = date("d/m/Y", strtotime(Mercado::where('telefone', "—")->max('created_at')));
        $controles['semFotos'] = Mercado::where('foto', 'imagens/mercado.png')->count();      
        $controles['dataFotos'] = date("d/m/Y", strtotime(Mercado::where('foto', 'imagens/mercado.png')->max('created_at')));
        return json_encode($controles);
    }

    public function desativar($id)
    {
        $fornecedor = Mercado::find($id);
        if (isset($fornecedor)) {
            $fornecedor->ativo = 1;
            $fornecedor->save();
        }
        return redirect('admin/fornecedores');
    }

    public function ativar($id)
    {
        $fornecedor = Mercado::find($id);
        if (isset($fornecedor)) {
            $fornecedor->ativo = 0;
            $fornecedor->save();
        }
        return redirect('admin/fornecedores');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function busca(Request $request)
    {
        $request->validate(['aPesquisar' => 'required']);
        $palavra = trim($request->input('aPesquisar'));
        return view('fornecedor-busca', compact('palavra'));
    }

    public function procurar(Request $request)
    {
        $request->validate(['p' => 'required']);
        $palavra = $request->get('p');
        $fornecedores = Mercado::where('razaosocial', 'like', '%' . $palavra . '%')->orderBy('razaosocial')->get();
        return json_encode($fornecedores);
    }
}
