<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use App\Pedido;
use App\Carrinho;
use App\Lanche;
use App\Contatosadmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->middleware('auth:admin');
    }

    public function index()
    {        
        if (date('m') == "01"){$mesabreviado = "JAN.";}
        if (date('m') == "02"){$mesabreviado = "FEV.";}     
        if (date('m') == "03"){$mesabreviado = "MAR.";}     
        if (date('m') == "04"){$mesabreviado = "ABR.";}     
        if (date('m') == "05"){$mesabreviado = "MAI.";}     
        if (date('m') == "06"){$mesabreviado = "JUN.";}     
        if (date('m') == "07"){$mesabreviado = "JUL.";}     
        if (date('m') == "08"){$mesabreviado = "AGO.";}     
        if (date('m') == "09"){$mesabreviado = "SET.";}     
        if (date('m') == "10"){$mesabreviado = "OUT.";}     
        if (date('m') == "11"){$mesabreviado = "NOV.";}     
        if (date('m') == "12"){$mesabreviado = "DEZ.";}       
        $data = date('d') . " " . $mesabreviado . " " . date('Y');
        return view('admin', compact('data'));
        /*
        if (Auth::check()) {
            $usuario = Auth::user();
            $contato = Contatosadmin::find($usuario->id);
            if (isset($contato)) {
                return view('admin', compact('data'));
            } else {
                $contato = new Contatosadmin();
                $contato->user_id = $usuario->id;
                $contato->created_at = new \DateTime();
                $contato->save();
                return view('admin', compact('data'));
            }
        }*/
    }

    public function novoPedido() {
        $proximo_pedido = Pedido::all()->count() + 1;
        //$lanches = Lanche::all()->where('ativo', 0);
        return view('pedido', compact('proximo_pedido'));
    }

    public function pedidos() {
        return $pedidos = Pedido::with(['lanches', 'usuario'])->where('ativo', 0)->orderBy('id', 'desc')->paginate(5);       
    }

    public function perfil($id)
    {
        $usuario = Admin::find($id);
        if (isset($usuario)) {
            return view('perfil', compact('usuario'));
        }
    }

    /**
     * METODO QUE ALTERA OS DADOS DO ADMIN E SEUS CONTATOS
     * @param \Illuminate\Http\Request  $request SAO OS DADOS DO FORM
     * @param int $id E O NUM. CORRESPONDENTE AO ADMIN A TER OS DADOS ALTERADOS
     * @return \Illuminate\Http\Response E A RESPOSTA ENVIADA P/ A VIEW
     */
    public function update(Request $request, $id)
    {
        $request->validate(['name' => 'required|string|min:3|max:30',
            'email' => 'required|string|email', 'celular1' => 'required',
            'cep' => 'required', 'rua' => 'required|string|max:30',
            'numero' => 'required|max:10', 'complemento' => 'max:30',
            'bairro' => 'required|string|max:30',
            'cidade' => 'required|string|max:30', 'uf' => 'required'],
            ['required' => 'O campo é obrigatório!',
                'min' => 'O campo deve ter mais que 2 letras!',
                'max' => 'O campo deve ser menor que 30 letras!',
                'email.email' => 'O e-mail não é válido!']);
        $usuario = Admin::find($id);
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $foto = $request->file('foto');
        if (isset($foto)) {
            if ($usuario->foto === "imagens/perfil.png") {
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            } else {
                Storage::disk('public')->delete($usuario->foto);
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            }
        }
        $usuario->save();
        $contatos = Contatosadmin::where('admin_id', $id)->get()->first();
        $contatos->cep = $request->input('cep');
        $contatos->rua = $request->input('rua');
        $contatos->numero = $request->input('numero');
        $contatos->complemento = $request->input('complemento') == null ? "—" : trim($request->input('complemento'));
        $contatos->bairro = $request->input('bairro');
        $contatos->cidade = $request->input('cidade');
        $contatos->uf = $request->input('uf');
        $contatos->celular1 = $request->input('celular1');
        $contatos->celular2 = $request->input('celular2') == null ? "—" : $request->input('celular2');
        $contatos->residencial = $request->input('residencial') == null ? "—" : $request->input('residencial');
        $contatos->facebook = $request->input('facebook') == null ? "—" : trim($request->input('facebook'));
        $contatos->twitter = $request->input('twitter') == null ? "—" : trim($request->input('twitter'));
        $contatos->instagram = $request->input('instagram') == null ? "—" : trim($request->input('instagram'));
        $contatos->save();
        $request->session()->flash('OK', ' DADOS ATUALIZADOS COM SUCESSO!');
        return redirect('admin/perfil/' . $id);
    }

    public function pedido(Request $request){
        //$pedidos = Pedido::with(['usuario'])->where('id', $request->get('id'))->where('ativo', 0)->get();
        return json_encode(Pedido::with(['usuario'])->where('id', $request->get('id'))->where('ativo', 0)->get());
        //return $pedidos->toJson();
    }

    public function encerrar($id)
    {
        $pedido = Pedido::find($id);
        if (isset($pedido)) {
            $pedido->ativo = 1;
            //ADD O STATOS DE ENCERRADO
            $pedido->save();
        }
        return redirect('admin');
    }

    public function editarPedido(Request $request, $id){
        //dd($request);
        $pedido = Pedido::find($id);
        if (isset($pedido)) {
            $pedido->status = $request->input('status');;
            //ADD O STATOS DE ENCERRADO
            $pedido->save();
            $request->session()->flash('OK', ' STATUS DO PEDIDO ALTERADO COM SUCESSO!');
        } else {
            $request->session()->flash('OK', ' ERRO!');
        }
        return redirect('admin');
    }

    public function lanche(Request $request){
        //$cliente = User::with(['contato'])->where('id', $request->get('id'))->get();
        return json_encode(Lanche::where('ativo', 0)->where('id', $request->get('id'))->get());        
    }
    public function lanchesPedido(Request $request){
        //$cliente = User::with(['contato'])->where('id', $request->get('id'))->get();
        //return json_encode(Lanche::where('ativo', 0)->where('id', $request->get('id'))->get());    
        $lanches = Pedido::with(['usuario', 'lanches'])->where('id', $request->get('id'))->where('ativo', 0)->get();
        return $lanches->toJson();
    }

    public function procurar(Request $request)
    {
        //dd($request);
        $request->validate(['p' => 'required']);
        $palavra = $request->get('p');
        //$lanche = La::with(['contato'])->where('name', 'like', '%' . $palavra . '%')->orderBy('name')->get();
        return json_encode(Lanche::where('nome', 'like', '%' . $palavra . '%')->orderBy('nome')->get());
        //return json_encode($clientes);
    }

    public function newPedido(Request $request)
    {
        //dd($request);
        $pedido = new Pedido();        
        $pedido->total = $request->get('totalfinal');
       $pedido->status = $request->get('status');
       $data = explode("/", $request->get('data'));
       $hora = date("H:i:s");
      $pedido->created_at = $data[2] . "-" . $data[1] . "-" . $data[0] . " " . $hora;
      $pedido->save();      
        
        $quantRequests = (count($request->all()));
        for ($i = 0; $i < $quantRequests; $i++) {           
            $carrinho = new Carrinho();
            $carrinho->pedido_id = Pedido::all()->count();
            $carrinho->user_id = $request->get('idcliente');           
            $lanche = Lanche::where('nome', $request->get('produto' . $i))->first();            
            if(isset($lanche)){
                $carrinho->quantidade = $request->get('quantidade' . $i);  
                $carrinho->lanche_id = $lanche->id;
                $carrinho->save();
                $request->session()->flash('OK', ' PEDIDO REALIZADO COM SUCESSO!');
            } 
        }
        return redirect('admin');
    }
}
