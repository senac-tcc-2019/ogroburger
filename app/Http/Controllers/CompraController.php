<?php

namespace App\Http\Controllers;

use App\Mercado;
use App\Produto;
use App\Compra;
use Illuminate\Http\Request;

class CompraController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fornecedores = Mercado::where('ativo', 0)->orderBy('razaosocial')->get();
        $produtos = Produto::where('ativo', 0)->orderBy('nome')->get();
        return view('compra', compact('fornecedores', 'produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['nf' => 'required'],
            ['required' => 'O campo é obrigatório!']);       
        //$nf = $request->get('nf');
        //$data = $request->get('data');
        $quantRequests = (count($request->all()) - 3);
        for ($i = 0; $i < $quantRequests; $i++) {            
            $produto = Produto::find($request->get('produto'.$i));            
            if (isset($produto)) {                
                $produto->quantidade = $produto->quantidade + $request->get('quantidade'.$i);
                $produto->save();     
                $compra = new Compra();
                $compra->mercado_id = $request->get('fornecedor');
                $compra->produto_id = $produto->id;           
                $compra->quantidade = $request->get('quantidade'.$i);
                $compra->custounitario = $request->get('custo'.$i);
                $compra->total = $request->get('total'.$i);
                $compra->save();
            }
        }
        $request->session()->flash('OK', ' COMPRA CADASTRADA COM SUCESSO!');
        //$fornecedores = Mercado::where('ativo', 0)->orderBy('razaosocial')->get();
        //$produtos = Produto::where('ativo', 0)->orderBy('nome')->get();
        return redirect('admin/produtos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
