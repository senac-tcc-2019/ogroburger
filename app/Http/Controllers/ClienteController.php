<?php

namespace App\Http\Controllers;

use App\Contatosuser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use PDF;
use Carbon\Carbon;

class ClienteController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $this->middleware('auth:admin');
    }
   
    public function index()
    {
        return view('clientes');
    }

    public function az()
    {
        return User::with(['contato'])->where('ativo', 0)->where('perfil', 'CLIENTE')->orderBy('name')->paginate(5);
    }

    public function za()
    {
        return User::with(['contato'])->where('ativo', 0)->where('perfil', 'CLIENTE')->orderBy('name', 'desc')->paginate(5);
    }

    public function pdfClientesAtivos(){
        $clientes = User::where('ativo', 0)->orderBy('name')->get();        
        return PDF::loadView('relatorios.pdf-clientes-ativos', ['clientes'=>$clientes])
        ->setPaper('a4')->stream('pdf-clientes-ativos.pdf');
    }

    public function pdfClientesDesativados(){
        $clientes = User::where('ativo', 1)->orderBy('name')->get();        
        return PDF::loadView('relatorios.pdf-clientes-desativados', ['clientes'=>$clientes])
        ->setPaper('a4')->stream('pdf-clientes-desativados.pdf');
    }

    public function pdfClientesAtivosDownload(){
        $clientes = User::where('ativo', 0)->orderBy('name')->get();
        return PDF::loadView('relatorios.pdf-clientes-ativos', ['clientes'=>$clientes])
        ->setPaper('a4')->download('pdf-clientes-ativos.pdf');
    }

    public function pdfClientesDesativadosDownload(){
        $clientes = User::where('ativo', 1)->orderBy('name')->get();
        return PDF::loadView('relatorios.pdf-clientes-desativados', ['clientes'=>$clientes])
        ->setPaper('a4')->download('pdf-clientes-desativados.pdf');
    }

    public function desativados()
    {
        $clientes = User::where('ativo', 1)->orderBy('name')->get();
        return json_encode($clientes);
    }

    public function controleClientes()
    {
        $controles['ativos'] = User::where('ativo', 0)->count();        
        $controles['ultimoCliente'] = Carbon::parse(User::max('created_at'))->format('d/m/Y');
        $controles['desativados'] = User::where('ativo', 1)->count();      
        $controles['ultimoDesativado'] = Carbon::parse(User::where('ativo', 1)->max('updated_at'))->format('d/m/Y');
        $controles['semEndereco'] = Contatosuser::where('rua', "—")->count();
        $controles['ultimoSemEndereco'] = Carbon::parse(Contatosuser::where('rua', "—")->max('created_at'))->format('d/m/Y');
        $controles['semFoto'] = User::where('foto', 'imagens/perfil.png')->count();      
        $controles['ultimoSemFoto'] = Carbon::parse(User::where('foto', 'imagens/perfil.png')->max('created_at'))->format('d/m/Y');
        return json_encode($controles);
    }

    public function desativar($id)
    {
        $usuario = User::find($id);
        if (isset($usuario)) {
            $usuario->ativo = 1;
            $usuario->save();
        }
        return redirect('admin/clientes');
    }

    public function ativar($id)
    {
        $usuario = User::find($id);
        if (isset($usuario)) {
            $usuario->ativo = 0;
            $usuario->save();
        }
        return redirect('admin/clientes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('cliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $usuario)
    {
        $request->validate(['name' => 'required'], ['required' => 'O campo é obrigatório!']);  
        //$id = User::all()->count() + 1;           
        $usuario->name = $request->input('name');      
        $usuario->email = $request->file('email') == null ? 
        $usuario->name . '' . rand(1,100) . rand(1,100) . '@ogroburger.com.br' : $request->file('email');
        $usuario->password = $request->file('senha') == null ? 
        Hash::make(rand(1,100) . rand(1,100) . rand(1,100) . rand(1,100) . rand(1,100) . rand(1,100)) :
        Hash::make($request->file('senha'));
        $usuario->perfil = 'DEFAULT';   
        $usuario->foto = $request->file('foto') == null ? 'imagens/perfil.png' : $request->file('foto')->store('imagens', 'public');
        $usuario->save();   
        $contatos = new Contatosuser();    
        $contatos->cep = $request->input('cep') == null ? "—" : $request->input('cep');
        $contatos->rua = $request->input('rua') == null ? "—" : $request->input('rua');
        $contatos->numero = $request->input('numero') == null ? "—" : $request->input('numero');
        $contatos->complemento = $request->input('complemento') == null ? "—" : $request->input('complemento');
        $contatos->bairro = $request->input('bairro') == null ? "—" : $request->input('bairro');
        $contatos->cidade = $request->input('cidade') == null ? "—" : $request->input('cidade');
        $contatos->uf = $request->input('uf') == null ? "—" : $request->input('uf');
        $contatos->celular1 = $request->input('celular1') == null ? "—" : $request->input('celular1');
        $contatos->celular2 = $request->input('celular2') == null ? "—" : $request->input('celular2');
        $contatos->residencial = $request->input('residencial') == null ? "—" : $request->input('residencial');
        $contatos->facebook = $request->input('facebook') == null ? "—" : $request->input('facebook');
        $contatos->twitter = $request->input('twitter') == null ? "—" : $request->input('twitter');
        $contatos->instagram = $request->input('instagram') == null ? "—" : $request->input('instagram');        
        $contatos->user_id = $usuario->id;     
        $contatos->save();
        $request->session()->flash('OK', ' CLIENTE CADASTRADO COM SUCESSO!');
        return redirect('admin/clientes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);
        if (isset($usuario)) {
            return view('cliente-editar', compact('usuario'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['name' => 'required|string|min:3|max:30',
            'email' => 'required|string|email', 'celular1' => 'required',
            'cep' => 'required', 'rua' => 'required|string|max:30',
            'numero' => 'required|max:10', 'complemento' => 'max:30',
            'bairro' => 'required|string|max:30',
            'cidade' => 'required|string|max:30', 'uf' => 'required'],
            ['required' => 'O campo é obrigatório!',
                'min' => 'O campo deve ter mais que 2 letras!',
                'max' => 'O campo deve ser menor que 30 letras!',
                'email.email' => 'O e-mail não é válido!']);
        $usuario = User::find($id);
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $foto = $request->file('foto');
        if (isset($foto)) {
            if ($usuario->foto === "imagens/perfil.png") {
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            } else {
                Storage::disk('public')->delete($usuario->foto);
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            }
        }
        $usuario->save();
        $contatos = Contatosuser::where('user_id', $id)->get()->first();
        $contatos->cep = $request->input('cep');
        $contatos->rua = $request->input('rua');
        $contatos->numero = $request->input('numero');
        $contatos->complemento = $request->input('complemento') == null ? "—" : trim($request->input('complemento'));
        $contatos->bairro = $request->input('bairro');
        $contatos->cidade = $request->input('cidade');
        $contatos->uf = $request->input('uf');
        $contatos->celular1 = $request->input('celular1');
        $contatos->celular2 = $request->input('celular2') == null ? "—" : $request->input('celular2');
        $contatos->residencial = $request->input('residencial') == null ? "—" : $request->input('residencial');
        $contatos->facebook = $request->input('facebook') == null ? "—" : trim($request->input('facebook'));
        $contatos->twitter = $request->input('twitter') == null ? "—" : trim($request->input('twitter'));
        $contatos->instagram = $request->input('instagram') == null ? "—" : trim($request->input('instagram'));
        $contatos->save();
        $request->session()->flash('OK', ' DADOS ATUALIZADOS COM SUCESSO!');
        return redirect('admin/clientes/editar/' . $id);
    }

    public function pesquisar(Request $request)
    {
        $request->validate(['aPesquisar' => 'required']);
        $palavra = trim($request->input('aPesquisar'));
        return view('cliente-busca', compact('palavra'));
    }

    public function procurar(Request $request)
    {
        $request->validate(['p' => 'required']);
        $palavra = $request->get('p');
        $clientes = User::with(['contato'])->where('name', 'like', '%' . $palavra . '%')->orderBy('name')->get();
        return json_encode($clientes);
    }

    public function perfil(Request $request){
        //$cliente = User::with(['contato'])->where('id', $request->get('id'))->get();
        return json_encode(User::with(['contato'])->where('id', $request->get('id'))->get());        
    }
}
