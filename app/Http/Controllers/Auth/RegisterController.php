<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data SAO VARIOS DADOS VINDOS DO FORMULARIO
     * @return \Illuminate\Contracts\Validation\Validator Q PODE SER true OU false
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|min:3|max:30',
            'sobrenome' => 'required|string|min:3|max:30',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required'],
            ['required' => 'O campo é obrigatório!',
                'min' => 'O campo deve ter mais que 2 letras!',
                'max' => 'O campo deve ser menor que 30 letras!',
                'email.email' => 'O e-mail não é válido!',
                'email.unique' => 'O e-mail já está cadastrado!',
                'password.min' => 'O campo deve ser maior que 5 digitos!',
                'password.confirmed' => 'As senhas não são iguais!',
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data SAO OS DADOS PASSADOS PELA VALIDACAO, OU SEJA, VALIDOS
     * @return \App\User UM USUARIO VALIDO P/ SER CADASTRADO NO BD
     */
    protected function create(array $data)
    {
        // METODO Q REMOVE OS ESPACOS NO INICIO E FIM DO NOME E SOBRENOME CASO EXISTA
        $data['name'] = trim($data['name']) . " " . trim($data['sobrenome']);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
