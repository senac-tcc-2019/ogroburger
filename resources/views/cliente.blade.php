@extends('layouts.dashboard', ["current" => "clientes"])
@section('conteudo')
<!-- PARTE DO CONTEUDO EM SI -->
<div class="page-wrapper">
    <!-- BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
    <div class="page-breadcrumb">
        <div class="row align-items-center">
            <div class="col-12">
                <h4 class="page-title">CLIENTE</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="\admin">Home (Dashboard)</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="\admin\clientes">Clientes</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Novo</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
    <!-- CONTEUDO FLUIDO  -->
    <div class="container-fluid">
        <!-- LINHA -->
        <div class="row">
            <!-- COLUNA DO FORMULARIO DE CRIACAO DE UM NOVO CLIENTE -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <div class="card">
                    <div class="card-header titulo-card">
                        <h5>CADASTRO DE NOVO CLIENTE</h5>
                        <div class="form-requerido">
                            <small><em>* O CAMPO É OBRIGATÓRIO!</em></small>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" novalidate action="/admin/cliente/novo"
                            class="form-horizontal form-material">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name"><strong>Nome</strong> <span
                                            class="form-requerido">*</span></label>
                                    <input type="text" name="name" value="{{old('name')}}"
                                        placeholder="Digite o nome completo" required id="name"
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>    
                                <div class="form-group col-md-6">
                                    <label for="residencial"><strong>Telefone residencial</strong></label>
                                    <input type="text" name="residencial" value="{{old('residencial')}}"
                                        placeholder="Digite o tel. residencial" class="form-control form-control-sm"
                                        id="residencial" />
                                </div>                            
                            </div>                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="celular1"><strong>Celular/WhatsApp</strong></label>
                                    <input type="text" name="celular1" value="{{old('celular1')}}"
                                        placeholder="Digite o celular/WhatsApp" id="celular1"
                                        class="form-control{{ $errors->has('celular1') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('celular1'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('celular1') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="celular2"><strong>Celular 2</strong></label>
                                    <input type="text" name="celular2" value="{{old('celular2')}}"
                                        placeholder="Digite outro celular" class="form-control form-control-sm"
                                        id="celular2" />
                                </div>                               
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="cep"><strong>CEP</strong></label>
                                    <input type="text" name="cep" value="{{old('cep')}}" placeholder="Digite o CEP"
                                        id="cep"
                                        class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('cep'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cep') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="rua"><strong>Rua</strong></label>
                                    <input type="text" name="rua" value="{{old('rua')}}" placeholder="Digite a rua"
                                        id="rua"
                                        class="form-control{{ $errors->has('rua') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('rua'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('rua') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="numero"><strong>Número</strong></label>
                                    <input type="number" name="numero" value="{{old('numero')}}"
                                        placeholder="Digite o n.°" id="numero"
                                        class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('numero'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label for="complemento"><strong>Complemento</strong></label>
                                    <input type="text" name="complemento" value="{{old('complemento')}}"
                                        placeholder="Digite o complemento" class="form-control form-control-sm"
                                        id="complemento" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="bairro"><strong>Bairro</strong></label>
                                    <input type="text" name="bairro" value="{{old('bairro')}}"
                                        placeholder="Digite o bairro" id="bairro"
                                        class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('bairro'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bairro') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cidade"><strong>Cidade</strong></label>
                                    <input type="text" name="cidade" value="{{old('cidade')}}"
                                        placeholder="Digite a cidade" id="cidade"
                                        class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('cidade'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cidade') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="uf"><strong>UF</strong></label>
                                    <input type="text" name="uf" value="{{old('uf')}}" placeholder="Digite a UF"
                                        id="uf"
                                        class="form-control{{ $errors->has('uf') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('uf'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('uf') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>                           
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="facebook"><strong>Facebook</strong></label>
                                    <input type="text" name="facebook" value="{{old('facebook')}}"
                                        placeholder="Digite a URL do Facebook" id="facebook"
                                        class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('facebook'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="twitter"><strong>Twitter</strong></label>
                                    <input type="text" name="twitter" value="{{old('twitter')}}"
                                        placeholder="Digite a URL do Twitter" id="twitter"
                                        class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('twitter'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="instagram"><strong>Instagram</strong></label>
                                    <input type="text" name="instagram" value="{{old('instagram')}}"
                                        placeholder="Digite a URL do Instagram" id="instagram"
                                        class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }} form-control-sm" />
                                    @if ($errors->has('instagram'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('instagram') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button class="btn btn-success btn-sm btn-espaco" type="submit">
                                    <i class="mdi mdi-content-save"></i> CADASTRAR CLIENTE</button>
                                <button class="btn btn-primary btn-sm btn-espaco" type="reset">
                                    <i class="mdi mdi-broom"></i> LIMPAR</button>
                                <a href="/admin/clientes" class="btn btn-secondary btn-sm btn-espaco">
                                    <i class="mdi mdi-close"></i> CANCELAR</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->
        </div>
        <!-- FIM DA LINHA -->
    </div>
    <!-- FIM DO CONTEUDO FLUIDO  -->
    <!-- AQUI TERIA Q TER UMA </div> PRA FECHAR A PARTE DO CONTEUDO EM SI
    MAS ELA ESTA NA PARTE DO LAYOUT DO DASHBOARD -->
    @endsection