<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard (Ogro Burger) — Relatório de clientes ativos</title>
    <style>
        header {
            line-height: 0.1
        }

        th {
            height: 40px;
            border-bottom: 1px solid #ddd;
        }

        td {
            height: 20px
        }

        tbody {
            font-size: 12px
        }

        .cliente {
            width: 110px
        }

        .email {
            width: 180px
        }

        .end {
            width: 210px
        }

        .bairro {
            width: 100px
        }

        footer {
            position: fixed;
            bottom: -50px;
            left: 0px;
            right: 0px;
            height: 50px;
            font-size: 12px;
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>

<body>
    <header>
        <img src="./img/logo-pdf.png" width="90" height="90" />
        <h1>Relatório de clientes ativos</h1>
        <h4>O sistema possui ({{count($clientes)}}) cliente(s) ativo(s) cadastrado(s)!</h4>
    </header>
    <table>
        <thead>
            <tr>
                <th class="cliente">CLIENTE</th>
                <th class="email">E-MAIL</th>
                <th class="end">ENDEREÇO</th>
                <th class="bairro">BAIRRO</th>
                <th>CELULAR</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clientes as $c)
            <tr>
                <td>
                    @if (Storage::exists($c->foto))
                    <img src="{{public_path('./storage/imagens/'.$c->foto)}}" style="width:50px;height:50px">
                    @else

                    @endif {{$c->name}}
                </td>
                <td>{{$c->email}}</td>
                <td>
                    @if($c->contato->rua != "—")
                    {{$c->contato->rua}}, n° {{$c->contato->numero}}
                    @else
                    —
                    @endif
                    @if($c->contato->complemento != "—")
                    — {{$c->contato->complemento}}
                    @else
                    @endif
                </td>
                <td>{{$c->contato->bairro}}</td>
                <td>{{$c->contato->celular1}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <footer>
        COPYRIGHT &copy; OGRO BURGER <?php echo date("Y");?> — ALGUNS DIREITOS RESERVADOS! BY <b>RTO</b> - TRABALHO
        FEITO COM S2
    </footer>
</body>

</html>