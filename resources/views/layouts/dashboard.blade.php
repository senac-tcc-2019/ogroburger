<!doctype html>
<html dir="ltr" lang="en">
   <head>
      <title>Dashboard - Ogro Burger - Hamburgueria</title>
      <meta charset="UTF-8">
      <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <!-- Favicon (ícone na aba do navegador) -->
      <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon"/>
      <link rel="shortcut icon" type="image/x-icon" href="{!! asset('favicon.ico') !!}" />
      <!-- Estilos (Bootstrap e personalizado) -->
      <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!--<link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">-->
   </head>
   <body>
      <!-- ESTILO DO TODO (PRINCIPAL) -->
      <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5"
         data-sidebartype="full" data-sidebar-position="absolute"
         data-header-position="absolute" data-boxed-layout="full">
         <!-- MENU SUPERIOR (CABECALHO) -->
         <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
               <div class="navbar-header" data-logobg="skin5">
                  <!-- LOGO DO CANTO SUPERIOR ESQUERDO (MENU SUPERIOR) -->
                  <a class="navbar-brand" href="/">
                     <!-- LOGO -->
                     <b class="logo-icon">
                     <img src="{!! asset('img/logo-dashboard.png') !!}"
                        class="light-logo" alt="homepage" />
                     </b>
                     <!-- TEXTO DO LADO DO LOGO -->
                     <span class="logo-text">
                     <img src="{!! asset('img/razao-social.png') !!}"
                        class="light-logo" alt="homepage" />
                     </span>
                  </a>
                  <!-- FIM DO LOGO -->
                  <!-- BOTAO QUE MOSTRA O MENU LATERAL, VISIVEL SOMENTE NO CELULAR!!! -->
                  <a class="nav-toggler waves-effect waves-light d-block d-md-none"
                     href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
               </div>
               <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                  <ul class="navbar-nav float-left mr-auto">
                     <!-- LUPA (PROCURA) Q FICA NO MENU SUPERIOR (CABECALHO) DO LADO ESQUERDO -->
                     <li class="nav-item search-box">
                        <!--<a class="nav-link waves-effect waves-dark"
                           href="javascript:void(0)"><i class="fas fa-search"></i></a>
                        <form class="app-search position-absolute">
                           <input type="text" class="form-control"
                              placeholder="Procurar &amp; enter"> <a class="srh-btn">
                           <i class="ti-close"></i></a>
                        </form>-->
                     </li>
                  </ul>
                  <!-- IMG DE PERFIL (MENU) DO LADO DIREITO DO MENU SUPERIOR (CABECALHO) -->
                  <ul class="navbar-nav float-right">
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic"
                           href="" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"><img src="/storage/{{ Auth::user()->foto }}"
                           alt="user" class="rounded-circle" width="31"></a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated">
                           @if(Auth::user()->perfil === "CLIENTE")
                           <a class="dropdown-item" href="/home/perfil/{{Auth::user()->id}}">
                           @else
                           <a class="dropdown-item" href="/admin/perfil/{{Auth::user()->id}}">
                           @endif
                           <i class="ti-user m-r-5 m-l-5"></i> MEU PERFIL</a>
                           <a class="dropdown-item" href="javascript:void(0)">
                           <i class="ti-wallet m-r-5 m-l-5"></i> MEU BALANÇO</a>
                           <a class="dropdown-item" href="javascript:void(0)">
                           <i class="ti-email m-r-5 m-l-5"></i> CAIXA DE ENTRADA</a>
                           <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                 <i class="fa fa-power-off m-r-5 m-l-5"></i> SAIR</a>
                                 <form id="logout-form" action="{{ route('logout') }}"
                                    method="POST" style="display: none;">
                                    @csrf
                           </form>
                        </div>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!-- FIM DO MENU A PARTIR DA IMG DE PERFIL Q FICA A DIREITA NO MENU SUPERIOR (CABECALHO) -->
         <!-- MENU LATERAL ESQUERDO  -->
         @if(Auth::user()->perfil === "CLIENTE")
         @component('componentes.menu-lateral-cliente', ["current" => $current])
         @endcomponent
         @else
         @component('componentes.menu-lateral', ["current" => $current])
         @endcomponent
         @endif
          <!-- FIM DO MENU LATERAL ESQUERDO -->
         @hasSection('conteudo')
         @yield('conteudo')
         @endif
         <!-- RODAPE -->
         <footer class="footer text-center">
         <small>COPYRIGHT &copy; <strong>OGRO BURGER</strong> 2018 — ALGUNS DIREITOS RESERVADOS! BY 
            <strong><a href="https://gitlab.com/rafaelcalearo" target="_blank">ЯTO</a></strong> 
            — TRABALHO FEITO COM <i class="fas fa-heart"></i></small>         
         </footer>
         <!-- FIM DO RODAPE -->
      </div>
      <!-- End Page wrapper  -->
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="{{ asset('js/app.js') }}"></script>
      <!--Wave Effects -->
      <script src="{{ asset('js/waves.js') }}"></script>
      <!--Menu sidebar -->
      <script src="{{ asset('js/sidebarmenu.js') }}"></script>
      <!-- MASCARAS DOS CAMPOS INPUTS -->
      <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>
      <!--Custom JavaScript -->
      <!--<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>-->
      <script src="{{ asset('js/custom.js') }}"></script>   
      <!--<script src="http://maps.google.com/maps?file=api&v=2&key=AIzaSyA53FKE3TYOq5c2127s6TkcilVngyfFves" type="text/javascript"></script>-->
      <!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA53FKE3TYOq5c2127s6TkcilVngyfFves" type="text/javascript"></script>-->
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA53FKE3TYOq5c2127s6TkcilVngyfFves"></script>
      @hasSection('js')
         @yield('js')
      @endif      
   </body>
</html>
