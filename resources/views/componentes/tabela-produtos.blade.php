<div class="card-body">
   <div class="d-md-flex">
      <div>
         <h4 class="card-title">PRODUTOS ATIVOS</h4>
         <h5 class="card-subtitle">Exebindo {{$produtos->count()}} 
         produto(s) de {{$produtos->total()}} 
            ({{$produtos->firstItem()}} a {{$produtos->lastItem()}}).
         </h5>
      </div>
      <div class="ml-auto d-flex no-block align-items-center">
         <ul class="list-inline font-12 dl m-r-5 m-b-3">
            <li class="list-inline-item"><i class="mdi mdi-pencil text-info"></i> EDIÇÃO DOS DADOS DO PRODUTO</li>
            <li class="list-inline-item"><i class="mdi mdi-block-helper text-danger"></i> DESATIVA O PRODUTO</li>
         </ul>
      </div>
   </div>
   <div class="d-md-flex justify-content-end">
      <ul class="list-inline m-r-5 m-b-0">
         <li class="tamanho-input-busca">
            <form method="POST" action="/admin/produtos/busca">
            @csrf
            <div class="input-group stylish-input-group">
               <input type="search" class="form-control form-control-sm"  
                  placeholder="PESQUISAR PRODUTO" name="aPesquisar" 
                  id="aPesquisar" requerid />
               <span class="input-group-addon">
               <button type="submit" id="pesquisar">
               <i class="mdi mdi-magnify"></i>
               </button>  
               </span>
            </div>
            </form>
         </li>
         <li></li>
      </ul>
   </div>
</div>
<div class="table-responsive tamanho-tbl">
   <table class="table v-middle text-nowrap">
      <thead>
         <tr class="bg-light">
            <th class="border-top-0">PRODUTO</th>
            <th class="border-top-0 text-right">QUANTIDADE</th>
            <th class="border-top-0 text-center">STATUS</th>
            <th class="border-top-0 text-center">CADASTRADO</th>
            <th class="border-top-0 text-center">ATUALIZADO</th>
            <th class="border-top-0 text-center">AÇÕES</th>
         </tr>
      </thead>
      <tbody>
         @foreach($produtos as $p)
         <tr>
            <td><a href="/admin/produtos/perfil/{{$p->id}}" class="text-secondary">
               <img src="/storage/{{$p->foto}}" class="rounded-circle" width="40" height="40" />
               &nbsp;<strong>{{$p->nome}}</strong></a>
            </td>
            <td class="text-right">{{$p->quantidade}}</td>
            <td class="text-center">{{$p->status}}</td>
            <td class="text-center">{{$p->created_at->format('d/m/Y H:m:s')}}</td>
            <!--<td class="text-center">{{date('d/m/Y H:m:s', strtotime($p->updated_at))}}</td>-->
            <td class="text-center">{{$p->updated_at->format('d/m/Y H:m:s')}}</td>
            <td class="text-center">            
               <a href="/admin/clienprodutostes/editar/{{$p->id}}" class="text-info" 
                  title="Editar"><i class="mdi mdi-pencil"></i></a>&nbsp;                                          
               <a href="/admin/produtos/{{$p->id}}" class="text-danger" title="Desativar"><i class="mdi mdi-block-helper"></i></a>                                           
            </td>
         </tr>
         @endforeach  
      </tbody>
   </table>
</div>
<div class="paginacao">
   {{$produtos->links()}}
</div>