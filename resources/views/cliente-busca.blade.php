@extends('layouts.dashboard', ["current" => "clientes"])
@section('conteudo')
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-md-5">
         <h4 class="page-title">CLIENTES</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/admin">Home (Dashboard)</a></li>
                  <li class="breadcrumb-item"><a href="/admin/clientes">Clientes</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Resultado da pesquisa</li>
               </ol>
            </nav>
         </div>
      </div>
      <div class="col-md-7">
         <div class="text-right upgrade-btn">
            <a href="/admin/clientes/novo" class="btn btn-success text-white"><i class="fa fa-plus-square"></i> NOVO CLIENTE</a>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="card">
            <input type="hidden" value="{{ $palavra }}" id="palavra" />
            <div class="card-body">
               <div class="d-md-flex">
                  <div>
                     <h4 class="card-title">RESULTADO DA PESQUISA</h4>
                     <h5 class="card-subtitle">Sua pesquisa por
                        "<strong><span id="pal"></span></strong>" retornou o total
                        de <span id="total"></span> cliente(s).
                     </h5>
                  </div>
                  <div class="ml-auto d-flex no-block align-items-center">
                     <ul class="list-inline font-12 dl m-r-5 m-b-3">
                        <li class="list-inline-item"><i class="mdi mdi-check text-success"></i> ATIVA O CLIENTE</li>
                        <li class="list-inline-item"><i class="mdi mdi-block-helper text-danger"></i> DESATIVA O CLIENTE</li>
                     </ul>
                  </div>
               </div>
               <div class="d-md-flex justify-content-end">
                  <ul class="list-inline m-r-5 m-b-0">
                     <li class="tamanho-input-busca">
                        <div class="input-group stylish-input-group">
                           <input type="search" class="form-control form-control-sm"
                              placeholder="PESQUISAR NOVAMENTE" name="aPesquisar"
                              id="aPesquisar" requerid />
                           <span class="input-group-addon">
                           <button type="submit" id="pesquisar">
                           <i class="mdi mdi-magnify"></i>
                           </button>
                           </span>
                        </div>
                     </li>
                     <li></li>
                  </ul>
               </div>
            </div>
            <div class="table-responsive tamanho-tbl">
               <table class="table v-middle text-nowrap">
                  <thead>
                     <tr class="bg-light">
                        <th class="border-top-0">CLIENTE</th>
                        <th class="border-top-0">E-MAIL</th>
                        <th class="border-top-0 text-center">TIPO</th>
                        <th class="border-top-0">ENDEREÇO</th>                        
                        <th class="border-top-0 text-center">CELULAR</th>
                        <th class="border-top-0 text-center">AÇÕES</th>
                     </tr>
                  </thead>
                  <tbody id="linha">
                  </tbody>
               </table>
               <div class="text-center mt-5 aviso-tabela"></div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog"
        aria-labelledby="TituloModalCentralizado" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TituloModalCentralizado">PERFIL COMPLETO DO CLIENTE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-auto text-center">
                                <span id="imagem-perfil"></span>
                                <p class="mt-2">
                                    <span id="facebook-perfil"></span>
                                    <span id="twitter-perfil"></span>
                                    <span id="instagram-perfil"></span>
                                </p>
                            </div>
                            <div class="col-md-auto">
                                <h4 class="card-title mb-1" id="nome-perfil"></h4>
                                <h6 class="card-subtitle text-muted mb-0"><em><span id="tipo-perfil"></span></em></h6>
                                <p class="mb-1"><strong>Pontos:</strong> <span id="pontos-perfil"></span></p>
                                <p class="text-sm">
                                    <strong>E-mail:</strong> <span id="email-perfil"></span><br />
                                    <strong>Celular 1:</strong> <span id="celular1-perfil"></span><br />
                                    <strong>Celular 2:</strong> <span id="celular2-perfil"></span><br />
                                    <strong>Tel. residencial:</strong> <span id="residencial-perfil"></span><br />
                                    <strong>CEP:</strong> <span id="cep-perfil"></span><br />
                                    <strong>End.:</strong> <span id="endereco-perfil"></span><br />
                                    <strong>Bairro:</strong> <span id="bairro-perfil"></span><br />
                                </p>
                            </div>
                            <div class="col-md-12">
                               <div id="map" style="height: 190px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span id="editar"></span>
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">FECHAR</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script type="text/javascript">

$(document).ready(function () {
    var p = $('#palavra').val();
    $.getJSON('/admin/clientes/procurar', {p}, function (data) {
        $('#total').html(data.length);
        $('#pal').html(p);
        $('#aPesquisar').val(p);
        if (data.length <= 0) {
            $('.aviso-tabela').append('<h3 class="card-title">NENHUM RESULTADO ENCONTRADO.</h3>' +
                '<h5 class="card-subtitle">Sua pesquisa por "<strong>' + p + '</strong>" não re' +
                'tornou nada.</h5><h5 class="card-subtitle">Use outros termos!</h5>');
        } else {
            for (i = 0; i < data.length; i++) {
               end = data[i].contato.rua == "—" ? "—" : data[i].contato.rua + ", " + data[i].contato.numero + 
            (data[i].contato.complemento == "—" ? " " : ", " + data[i].contato.complemento) + " — " + 
            data[i].contato.bairro;
               if(data[i].ativo == 0){
                  acao = '<a href="/admin/clientes/desativar/' + data[i].id + '" class="text-danger" ' +
                  'title="Desativar" onclick="return confirm(\'Confirmar o desativamento de ' + 
                  data[i].name + '?\');"><i class="mdi mdi-block-helper"></i></a>';
               } else {
                  acao = '<a href="/admin/clientes/ativar/' + data[i].id + '" class="text-success" ' +
                  'title="Ativar" onclick="return confirm(\'Confirmar o ativamento de ' + 
                  data[i].name + '?\');"><i class="mdi mdi-check"></i></a>';
               }
               $('#linha').append('<tr><td id="perfil"><a href="javascript:void(0);" class="text-secondary" perfil="' + data[i].id + '"><img src="/storage/' + data[i].foto +
                '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;<strong>' + data[i].name +
                '</strong></a></td><td>' + data[i].email + '</td><td class="text-center">'+data[i].perfil+'</td><td>' + end +
                '</td><td class="text-center">' +
                data[i].contato.celular1 + '</td><td class="text-center">' + acao + '</td></tr>');
            }
            $("#perfil>a").click(function (e) {
                    e.preventDefault();
                    carregaPerfil($(this).attr('perfil'));
            })
        }
    }); // window.history.pushState("", "", url);
});

function carregaPerfil(id) {
            $.getJSON('/admin/clientes/perfil', { id }, function (data) {
               $('#imagem-perfil').html('<img src="/storage/' + data[0].foto + '" class="rounded-circle" width="150" height="150" />');
                $('#nome-perfil').html(data[0].name);
                $('#tipo-perfil').html(data[0].perfil);
                $('#pontos-perfil').html(data[0].pontos);
                $('#email-perfil').html(data[0].email);
                $('#celular1-perfil').html(data[0].contato.celular1);
                $('#celular2-perfil').html(data[0].contato.celular2);
                $('#residencial-perfil').html(data[0].contato.residencial);
                $('#cep-perfil').html(data[0].contato.cep);
                endereco = data[0].contato.rua != "—" ? data[0].contato.rua + ", " + data[0].contato.numero : data[0].contato.rua;
                complemento = data[0].contato.complemento == "—" ? " " : " — " + data[0].contato.complemento;
                $('#endereco-perfil').html(endereco + '' + complemento);
                bairro = data[0].contato.bairro == "—" ? "—" : data[0].contato.bairro + ', ' + data[0].contato.cidade + ' (' + data[0].contato.uf + ')';
                $('#bairro-perfil').html(bairro);
                $('#facebook-perfil').html(data[0].contato.facebook == "—" ? "" : '<a href="' + data[0].contato.facebook +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook"></i></a>');
                $('#twitter-perfil').html(data[0].contato.twitter == "—" ? "" : '<a href="' + data[0].contato.twitter +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a>');
                $('#instagram-perfil').html(data[0].contato.instagram == "—" ? "" : '<a href="' + data[0].contato.instagram +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fab fa-instagram"></i></a>');
                $('#editar').html('<a href="/admin/clientes/editar/' + id + '" class="btn btn-primary btn-sm">EDITAR</a>');                
                $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA53FKE3TYOq5c2127s6TkcilVngyfFves&address=' +
                    data[0].contato.bairro + ", " + data[0].contato.cep, dataType: 'json',
                    success: function (data) {
                        if (data.results.length > 0) {
                            var uluru = {
                                lat: parseFloat(data.results[0].geometry.location.lat),
                                lng: parseFloat(data.results[0].geometry.location.lng)
                            };
                            var map = new google.maps.Map(document.getElementById('map'), { zoom: 15, center: uluru });
                            var marker = new google.maps.Marker({ position: uluru, map: map });
                        } else {
                            var uluru = {
                                lat: -31.7726923,
                                lng: -52.3453726
                            };
                            var map = new google.maps.Map(document.getElementById('map'), { zoom: 15, center: uluru });
                            var marker = new google.maps.Marker({ position: uluru, map: map });
                        }
                    }
                });
                $("#ExemploModalCentralizado").modal('show');
            });
        }

$(function () {
    $('body').on('click', '#pesquisar', function (e) {
        e.preventDefault();
        var p = $('#aPesquisar').val();
        $('#palavra').val(p);
        $.getJSON('/admin/clientes/procurar', {p}, function (data) {
            $('#total').html(data.length);
            $('#pal').html(p);
            if (data.length <= 0) {
                $('#linha>tr').remove();
                $('.aviso-tabela').append('<h3 class="card-title">NENHUM RESULTADO ENCONTRADO.</h3>' +
                    '<h5 class="card-subtitle">Sua busca por "<strong>' + p + '</strong>" não retornou nada.</h5>' +
                    '<h5 class="card-subtitle">Use outros termos!</h5>');
            } else {
                $('.aviso-tabela').remove();
                $('#linha>tr').remove();
                for (i = 0; i < data.length; i++) {
                  end = data[i].contato.rua == "—" ? "—" : data[i].contato.rua + ", " + data[i].contato.numero + 
            (data[i].contato.complemento == "—" ? " " : ", " + data[i].contato.complemento) + " — " + 
            data[i].contato.bairro;
               if(data[i].ativo == 0){
                  acao = '<a href="/admin/clientes/desativar/' + data[i].id + '" class="text-danger" ' +
                  'title="Desativar" onclick="return confirm(\'Confirmar o desativamento de ' + 
                  data[i].name + '?\');"><i class="mdi mdi-block-helper"></i></a>';
               } else {
                  acao = '<a href="/admin/clientes/ativar/' + data[i].id + '" class="text-success" ' +
                  'title="Ativar" onclick="return confirm(\'Confirmar o ativamento de ' + 
                  data[i].name + '?\');"><i class="mdi mdi-check"></i></a>';
               }
               $('#linha').append('<tr><td id="perfil"><a href="javascript:void(0);" class="text-secondary" perfil="' + data[i].id + '"><img src="/storage/' + data[i].foto +
                '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;<strong>' + data[i].name +
                '</strong></a></td><td>' + data[i].email + '</td><td class="text-center">'+data[i].perfil+'</td><td>' + end +
                '</td><td class="text-center">' +
                data[i].contato.celular1 + '</td><td class="text-center">' + acao + '</td></tr>');
               }
               $("#perfil>a").click(function (e) {
                    e.preventDefault();
                    carregaPerfil($(this).attr('perfil'));
         })  
            }
        });            
    });   
});
</script>
@endsection
