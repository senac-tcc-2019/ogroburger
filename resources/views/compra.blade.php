@extends('layouts.dashboard', ["current" => "compras"])
@section('conteudo')
<div class="page-wrapper">
    <!-- PARTE DO CONTEUDO EM SI -->
    <div class="page-breadcrumb">
        <!-- BARRA DE CAMINHO (ONDE ESTOU!)" -->
        <div class="row align-items-center">
            <div class="col-12">
                <h4 class="page-title">COMPRA</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="\admin">Home (Dashboard)</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Compra</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div><!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) -->
    <div class="container-fluid">
        <!-- CONTEUDO FLUIDO  -->
        <div class="row">
            <!-- LINHA -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <!-- COLUNA DO FORM DE CRIACAO DE UMA NOVA COMPRA -->
                <div class="card">
                    <div class="card-header titulo-card">
                        <h5>CADASTRO DE NOVA COMPRA</h5>
                        <div class="form-requerido">
                            <small><em>* O CAMPO É OBRIGATÓRIO!</em></small>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="/admin/compras/nova" method="POST" class="form-horizontal form-material">
                            @csrf
                            <h6 class="card-title mt-4"><strong>INFORMAÇÕES DA COMPRA</strong></h6>
                            <div class="borda">
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="fornecedor"><strong>Fornecedor</strong>
                                                <span class="form-requerido">*</span></label>
                                            <select name="fornecedor" class="form-control">
                                                @foreach($fornecedores as $f)
                                                <option value="{{$f->id}}">{{$f->razaosocial}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="nf"><strong>N.º do cupom/nota fiscal</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" name="nf" placeholder="Ex.: 018931"
                                                class="form-control{{ $errors->has('nf') ? ' is-invalid' : '' }}"
                                                id="nf" />
                                            @if ($errors->has('nf'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('nf') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="data"><strong>Data da compra</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="date" name="data" value="{{old('data')}}"
                                                class="form-control{{ $errors->has('data') ? ' is-invalid' : '' }}"
                                                id="data" />
                                            @if ($errors->has('data'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('data') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6 class="card-title mt-4"><strong>PRODUTO(S) DA COMPRA</strong></h6>
                            <div class="borda">
                                <div class="col-md-12 text-right mb-2">
                                    <button type="button" disabled="disabled" class="btn btn-info btn-sm"
                                        id="adicionar">ADICIONAR PRODUTO</button>
                                </div>
                                <div id="quadro">
                                    <div class="form-row" id="linha0">
                                        <div class="form-group col-md-3">
                                            <label for="produto0"><strong>Produto</strong>
                                                <span class="form-requerido">*</span></label>
                                            <select name="produto0" class="form-control" id="produto0">
                                                @foreach($produtos as $p)
                                                <option value="{{$p->id}}">{{$p->nome}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="quantidade0"><strong>Quantidade</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" name="quantidade0" placeholder="Ex.: 2"
                                                class="form-control quantidade" id="quantidade0" />
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="custo0"><strong>Custo un.</strong>
                                                <span class="form-requerido">*</span>
                                                <!-- VER A CLASS "aviso-senha"! -->
                                                <span class="aviso-senha">· 0.00</span></label>
                                            <input type="text" name="custo0" placeholder="Ex.: 1.99"
                                                class="form-control" id="custo0" />
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="total0"><strong>Total</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" name="total0" value="0" readonly="readonly"
                                                class="form-control" id="total0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-4">
                                <h6><strong>QTD. TOTAL DE PRODUTOS:</strong> <span id="totalProdutos">0</span></h6>
                                <h4><strong>TOTAL DA COMPRA:</strong> <span id="totalCompra">R$ 0,00</span></h4>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="submit" disabled="disabled" class="btn btn-success btn-sm btn-espaco"
                                    id="cadastrar"><i class="mdi mdi-content-save"></i> CADASTRAR COMPRA</button>
                                <a href="/admin/compras" class="btn btn-secondary btn-sm btn-espaco">
                                    <i class="mdi mdi-close"></i> CANCELAR</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->
        </div><!-- FIM DA LINHA -->
    </div><!-- FIM DO CONTEUDO FLUIDO  -->
    <!-- AQUI TERIA Q TER UMA </div> P/ FECHAR A PARTE DO CONTEUDO EM SI, MAS ELA ESTA NO LAYOUT DO DASHBOARD -->
    @endsection
    @if (session('OK'))
    <div class="alert alerta-sucesso alert-dismissible" role="alert">
        <i class="fas fa-check-circle"></i>{{ session('OK') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @section('js')
    <script type="text/javascript">
        $(document).ready(function () {

            var i = 0;
            var totais = [];

            $('#quantidade' + i).keyup(function () {
                $(this).val(this.value.replace(/\D/g, ''));
            });

            $('#custo' + i).keyup(function () {
                if ($('#custo' + i).val().split(".")) {
                    var parte = $('#custo' + i).val().split(".");
                    var valor = parte[0] + "." + parte[1];
                    totais[i] = valor * $('#quantidade' + i).val();                    
                    if (totais[i] > 0 || totais[i] > 0.00) {
                        $('#total' + i).val(totais[i].toFixed(2).replace('.', '.'));
                        $('#totalProdutos').html(totais.length);
                        $('#totalCompra').html('R$ ' + totais[i].toFixed(2).replace('.', ','));
                        $('#adicionar').add('#cadastrar').attr("disabled", false);
                    } else {
                        $('#adicionar').add('#cadastrar').attr("disabled", true);
                        $('#total' + i).val(0);
                    }
                }
            });

            $('#adicionar').click(function (e) {
                e.preventDefault();
                $('#adicionar').add('#cadastrar').attr("disabled", true);
                $("#produto" + i).css("background", "#e9ecef", "pointer-events", "none", "touch-action", "none");
                $('#produto' + i + ' option:not(:selected)').prop('disabled', true);
                $('#quantidade' + i).add('#custo' + i).prop("readonly", true);
                i++;
                $("#quadro").append('<div class="form-row" id="linha' + i + '"><div class="form-group ' +
                    'col-md-3"><label for="produto' + i + '"><strong>Produto</strong> <span class="for' +
                    'm-requerido">*</span></label><select name="produto' + i + '" class="form-control"' +
                    ' id="produto' + i + '">@foreach($produtos as $p)<option value="{{$p->id}}">{{$p->nome}}' +
                    '</option>@endforeach</select></div><div class="form-group col-md-2"><label ' +
                    'for="quantidade' + i + '"><strong>Quantidade</strong> <span class="form-requerido' +
                    '">*</span></label><input type="number" name="quantidade' + i + '" placeholder="Ex' +
                    '.: 2" class="form-control" id="quantidade' + i + '" /></div><div class="form-grou' +
                    'p col-md-3"><label for="custo' + i + '"><strong>Custo un.</strong> <span class="f' +
                    'orm-requerido">*</span><span class="aviso-senha">· 0.00</span></label><input type' +  
                    '="text" name="custo' + i + '" placeholder="Ex.: 1.99" class="form-control" id="cu' + 
                    'sto' + i + '"/></div><div class="form-group col-md-2"><label for="total' + i + '"' +
                    '><strong>Total</strong> <span class="form-requerido">*</span></label><input type=' +
                    '"text" name="total' + i + '" readonly="readonly" value="0" class="form-control" i' +
                    'd="total' + i + '" /></div><div class="form-group col-md-2"><label><strong>Ação</' +
                    'strong></label><div><button type="button" disabled="disabled" class="btn btn-dang' +
                    'er btn-block" id="remover' + i + '">REMOVER</button></div></div>');

                $('#remover' + i).attr("disabled", false);

                totais[i] = 0;

                $('#quantidade' + i).keyup(function () {
                    $(this).val(this.value.replace(/\D/g, ''));
                });

                $('#custo' + i).keyup(function () {
                    if ($('#custo' + i).val().split(".")) {
                        var parte = $('#custo' + i).val().split(".");
                        var valor = parte[0] + "." + parte[1];
                        totais[i] = valor * $('#quantidade' + i).val();
                        if (totais[i] > 0 || totais[i] > 0.00) {
                            $('#total' + i).val(totais[i].toFixed(2).replace('.', '.'));
                            $('#totalProdutos').html(totais.length);
                            var totalCompra = 0;
                            for (var y = 0; y < totais.length; y++) {
                                totalCompra += totais[y];
                            }
                            $('#totalCompra').html('R$ ' + totalCompra.toFixed(2).replace('.', ','));
                            $('#adicionar').add('#cadastrar').attr("disabled", false);
                            $('#remover' + i).attr("disabled", true);
                        } else {
                            $('#adicionar').add('#cadastrar').attr("disabled", true);
                            $('#total' + i).val(0);
                            $('#remover' + i).attr("disabled", false);
                        }
                    }
                });

                $('#remover' + i).click(function (e) {
                    e.preventDefault();
                    $('#linha' + i).remove();
                    totais.pop();
                    var resto = 0;
                    for (var z = 0; z < i; z++) {
                        resto += totais[z];
                    }
                    $('#totalProdutos').html(totais.length);
                    $('#totalCompra').html('R$ ' + resto.toFixed(2).replace('.', ','));
                    i = i - 1;
                    $('#adicionar').add('#cadastrar').attr("disabled", false);
                });
            });
        });
    </script>
    @endsection