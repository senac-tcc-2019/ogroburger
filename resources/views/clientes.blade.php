@extends('layouts.dashboard', ["current" => "clientes"])

@section('conteudo')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-md-5">
                <h4 class="page-title">CLIENTES</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="/admin">Home (Dashboard)</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Clientes</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-7">
                <div class="text-right upgrade-btn">
                    <a href="/admin/cliente/novo" class="btn btn-success text-white">
                        <i class="fa fa-plus-square"></i> NOVO CLIENTE</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex">
                            <div>
                                <h4 class="card-title">CLIENTE(S) ATIVO(S)</h4>
                                <h5 class="card-subtitle subtitulo"></h5>
                            </div>
                            <div class="ml-auto d-flex no-block align-items-center">
                                <ul class="list-inline font-12 dl m-r-5 m-b-3">
                                    <li class="list-inline-item"><i class="mdi mdi-block-helper text-danger">
                                        </i> DESATIVA O CLIENTE</li>
                                </ul>
                            </div>
                        </div>
                        <div class="d-md-flex justify-content-end">
                            <ul class="list-inline m-r-5">
                                <li class="tamanho-input-busca">
                                    <form method="POST" action="/admin/clientes/pesquisar">
                                        @csrf
                                        <div class="input-group stylish-input-group">
                                            <input type="search" class="form-control form-control-sm"
                                                placeholder="PESQUISAR CLIENTE" name="aPesquisar" id="aPesquisar"
                                                requerid />
                                            <span class="input-group-addon">
                                                <button type="submit" id="pesquisar">
                                                    <i class="mdi mdi-magnify"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                            <div class="dropdown m-r-5">
                                <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    FILTRAR POR
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <h6 class="dropdown-header">TIPO DE CLIENTE</h6>
                                    <button class="dropdown-item" type="button" id="default">DEFAULT</button>
                                    <button class="dropdown-item" type="button" id="cliente">CLIENTE</button>
                                </div>
                            </div>
                            <div class="dropdown m-r-5">
                                <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ORDENAR POR
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <h6 class="dropdown-header">CLIENTES</h6>
                                    <button class="dropdown-item" type="button" id="az">De A–Z</button>
                                    <button class="dropdown-item" type="button" id="za">De Z–A</button>
                                </div>
                            </div>
                            <div class="dropdown m-r-5">
                                <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    GERAR RELATÓRIOS
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <h6 class="dropdown-header">VISUALIZAR PDF</h6>
                                    <a class="dropdown-item" href="/admin/clientes/pdf-clientes-ativos"
                                        target="_blank">Clientes ativos</a>
                                    <a class="dropdown-item" href="/admin/clientes/pdf-clientes-desativados"
                                        target="_blank">Clientes desativados</a>
                                    <div class="dropdown-divider"></div>
                                    <h6 class="dropdown-header">BAIXAR PDF</h6>
                                    <a class="dropdown-item"
                                        href="/admin/clientes/pdf-clientes-ativos-download">Clientes ativos</a>
                                    <a class="dropdown-item"
                                        href="/admin/clientes/pdf-clientes-desativados-download">Clientes
                                        desativados</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table v-middle text-nowrap" id="tabelaClientes">
                            <thead>
                                <tr class="bg-light">
                                    <th class="border-top-0">CLIENTE</th>
                                    <th class="border-top-0">E-MAIL</th>
                                    <th class="border-top-0 text-center">TIPO</th>
                                    <th class="border-top-0">ENDEREÇO</th>
                                    <th class="border-top-0 text-center">CELULAR</th>
                                    <th class="border-top-0 text-center">AÇÃO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- ITENS DA TBL INSERIDOS DINAMICAMENTE PELO JS -->
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <nav id="paginationNav">
                            <ul class="pagination pagination-sm justify-content-center">
                                <!-- ITENS DA PAGINACAO INSERIDOS DINAMICAMENTE PELO JS -->
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex">
                            <div>
                                <h4 class="card-title">CLIENTES DESATIVADOS</h4>
                                <h5 class="card-subtitle">Exibindo o total de
                                    <span id="total"></span> cliente(s) desativado(s).</h5>
                            </div>
                            <div class="ml-auto d-flex no-block align-items-center">
                                <ul class="list-inline font-12 dl m-r-15 m-b-0">
                                    <li class="list-inline-item">
                                        <i class="mdi mdi-check text-success"></i> ATIVA O CLIENTE</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row o-auto" style="height: 250px">
                            <div class="table-responsive mt-3">
                                <table class="table v-middle text-nowrap">
                                    <tbody id="desativados">
                                        <!-- ITENS DA TBL INSERIDOS DINAMICAMENTE PELO JS -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">CONTROLE DE CLIENTES</h4>
                        <h5 class="card-subtitle">Exibindo os totais dos indicadores listados abaixo</h5>
                        <div class="feed-widget" style="height: 250px">
                            <ul class="list-style-none feed-body m-0 p-b-20">
                                <li class="feed-item">
                                    <i class="mdi mdi-check text-success"></i>&nbsp;<strong>CLIENTES:</strong>
                                    &nbsp;<span id="clientes-ativos"></span>
                                    <span class="ml-auto font-12 text-muted"><span id="ultimo-cliente"></span></span>
                                </li>
                                <li class="feed-item">
                                    <i class="mdi mdi-block-helper text-danger"></i>&nbsp;<strong>DESATIVADOS:</strong>
                                    &nbsp;<span id="clientes-desativados"></span>
                                    <span class="ml-auto font-12 text-muted"><span id="ultimo-desativado"></span></span>
                                </li>
                                <li class="feed-item">
                                    <i class="mdi mdi-account-alert text-info"></i>&nbsp;<strong>DEFAULT:</strong>
                                    &nbsp;<span id="sem-endereco"></span>
                                    <span class="ml-auto font-12 text-muted"><span
                                            id="ultimo-sem-endereco"></span></span>
                                </li>
                                <li class="feed-item">
                                    <i class="mdi mdi-camera-off text-warning"></i>&nbsp;<strong>SEM FOTO:</strong>
                                    &nbsp;<span id="sem-foto"></span>
                                    <span class="ml-auto font-12 text-muted"><span id="ultimo-sem-foto"></span></span>
                                </li>
                                <li class="feed-item">
                                    <i class="mdi mdi-account-multiple text-primary"></i>&nbsp;<strong>TOTAL:</strong>
                                    &nbsp;<span id="total-clientes"></span>
                                    <span
                                        class="ml-auto font-12 text-muted"><span><?php echo date('d/m/Y');?></span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog"
        aria-labelledby="TituloModalCentralizado" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TituloModalCentralizado">PERFIL COMPLETO DO CLIENTE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-auto text-center">
                                <span id="imagem-perfil"></span>
                                <p class="mt-2">
                                    <span id="facebook-perfil"></span>
                                    <span id="twitter-perfil"></span>
                                    <span id="instagram-perfil"></span>
                                </p>
                            </div>
                            <div class="col-md-auto">
                                <h4 class="card-title mb-1" id="nome-perfil"></h4>
                                <h6 class="card-subtitle text-muted mb-0"><em><span id="tipo-perfil"></span></em></h6>
                                <p class="mb-1"><strong>Pontos:</strong> <span id="pontos-perfil"></span></p>
                                <p class="text-sm">
                                    <strong>E-mail:</strong> <span id="email-perfil"></span><br />
                                    <strong>Celular 1:</strong> <span id="celular1-perfil"></span><br />
                                    <strong>Celular 2:</strong> <span id="celular2-perfil"></span><br />
                                    <strong>Tel. residencial:</strong> <span id="residencial-perfil"></span><br />
                                    <strong>CEP:</strong> <span id="cep-perfil"></span><br />
                                    <strong>End.:</strong> <span id="endereco-perfil"></span><br />
                                    <strong>Bairro:</strong> <span id="bairro-perfil"></span><br />
                                </p>
                            </div>
                            <div class="col-md-12">
                               <div id="map" style="height: 190px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span id="editar"></span>
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">FECHAR</button>
                </div>
            </div>
        </div>
    </div>
    @if (session('OK'))
    <div class="alert alerta-sucesso alert-dismissible" role="alert">
        <i class="fas fa-check-circle"></i>{{ session('OK') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @endsection

    @section('js')
    <script type="text/javascript">

        var filtro = '';

        function getNextItem(data) {
            i = data.current_page + 1;
            if (data.current_page == data.last_page)
                s = '<li class="page-item disabled">';
            else
                s = '<li class="page-item">';
            s += '<a class="page-link" pagina="' + i + '" href="javascript:void(0);">Próximo</a></li>';
            return s;
        }

        function getPreviousItem(data) {
            i = data.current_page - 1;
            if (data.current_page == 1)
                s = '<li class="page-item disabled">';
            else
                s = '<li class="page-item">';
            s += '<a class="page-link" pagina="' + i + '" href="javascript:void(0);">Anterior</a></li>';
            return s;
        }

        function getItem(data, i) {
            if (data.current_page == i)
                s = '<li class="page-item active">';
            else
                s = '<li class="page-item">';
            s += '<a class="page-link" pagina="' + i + '" href="javascript:void(0);">' + i + '</a></li>';
            return s;
        }

        function montarPaginator(data) {
            $("#paginationNav>ul>li").remove();
            $("#paginationNav>ul").append(getPreviousItem(data));
            if ((data.total % 5) > 0) {
                n = parseInt((data.total / 5)) + 1;
            } else {
                n = data.total / 5;
            }
            if (data.current_page - n / 2 <= 1)
                inicio = 1;
            else if (data.last_page - data.current_page < n)
                inicio = data.last_page - n + 1;
            else
                inicio = data.current_page - n / 2;

            fim = inicio + n - 1;

            for (i = inicio; i <= fim; i++) {
                $("#paginationNav>ul").append(getItem(data, i));
            }
            $("#paginationNav>ul").append(getNextItem(data));
        }

        /*
        function montarLinha(linha) {
            end = linha.contato.rua == "—" ? "—" : linha.contato.rua + ", " + linha.contato.numero + 
            (linha.contato.complemento == "—" ? " " : ", " + linha.contato.complemento) + " — " + 
            linha.contato.bairro;
            return '<tr><td id="perfil"><a href="javascript:void(0);" class="text-secondary" perfil="' +
                linha.id + '"><img src="/storage/' + linha.foto + '" class="rounded-circle" width="40' +
                '" height="40" />&nbsp;&nbsp;<strong>' + linha.name + '</strong></a></td><td>' +
                linha.email + '</td><td class="text-center">' + linha.perfil + '</td><td>' + end + 
                '</td><td class="text-center">' + linha.contato.celular1 + '</td><td class="text-cent' +
                'er"><a href="/admin/clientes/desativar/' + linha.id + '" class="text-danger" title="' +
                'Desativar" onclick="return confirm(\'Confirmar o desativamento de ' +
                linha.name + '?\');"><i class="mdi mdi-block-helper"></i></a></td></tr>';
        }

        function montarTabela(data) {
            $("#tabelaClientes>tbody>tr").remove();
            for (i = 0; i < data.data.length; i++) {
                $("#tabelaClientes>tbody").append(
                    montarLinha(data.data[i])
                );
            }
        }
        */

        function montarTabela(data) {
            $("#tabelaClientes>tbody>tr").remove();
            for (i = 0; i < data.data.length; i++) {
                end = data.data[i].contato.rua == "—" ? "—" : data.data[i].contato.rua + ", " + data.data[i].contato.numero + 
            (data.data[i].contato.complemento == "—" ? " " : ", " + data.data[i].contato.complemento) + " — " + 
            data.data[i].contato.bairro;
                $("#tabelaClientes>tbody").append('<tr><td id="perfil"><a href="javascript:void(0);" class="text-secondary" perfil="' +
                data.data[i].id + '"><img src="/storage/' + data.data[i].foto + '" class="rounded-circle" width="40' +
                '" height="40" />&nbsp;&nbsp;<strong>' + data.data[i].name + '</strong></a></td><td>' +
                data.data[i].email + '</td><td class="text-center">' + data.data[i].perfil + '</td><td>' + end + 
                '</td><td class="text-center">' + data.data[i].contato.celular1 + '</td><td class="text-cent' +
                'er"><a href="/admin/clientes/desativar/' + data.data[i].id + '" class="text-danger" title="' +
                'Desativar" onclick="return confirm(\'Confirmar o desativamento de ' +
                data.data[i].name + '?\');"><i class="mdi mdi-block-helper"></i></a></td></tr>');
            }
        }

        function carregarAZ(pagina) {
            $("#az").css('font-weight', 'bolder');
            $("#za").css('font-weight', 'normal');
            $.get('/admin/clientes/az', { page: pagina }, function (resp) {
                montarTabela(resp);
                montarPaginator(resp);
                filtro = '';
                $("#paginationNav>ul>li>a").click(function () {
                    carregarAZ($(this).attr('pagina'));
                })
                $(".subtitulo").html("Exibindo " + resp.data.length + " cliente(s) de " + resp.total +
                    " (" + resp.from + " a " + resp.to + ")");
                $("#perfil>a").click(function (e) {
                    e.preventDefault();
                    carregaPerfil($(this).attr('perfil'));
                })
            });
        }

        function carregarZA(pagina) {
            $("#za").css('font-weight', 'bolder');
            $("#az").css('font-weight', 'normal');
            $.get('/admin/clientes/za', { page: pagina }, function (resp) {
                montarTabela(resp);
                montarPaginator(resp);
                filtro = 'za';
                $("#paginationNav>ul>li>a").click(function () {
                    carregarZA($(this).attr('pagina'));
                })
                $(".subtitulo").html("Exibindo " + resp.per_page + " cliente(s) de " + resp.total +
                    " (" + resp.from + " a " + resp.to + ")");
                $("#perfil>a").click(function (e) {
                    e.preventDefault();
                    carregaPerfil($(this).attr('perfil'));
                })
            });
        }

        function carregaDesativados() {
            $.getJSON('/admin/clientes/desativados', function (data) {
                for (i = 0; i < data.length; i++) {
                    var linha = '<tr><td id="perfil"><a href="javascript:void(0);" class="text-secondary" perfil="' + data[i].id + '"><img src="/storage/' +
                        data[i].foto + '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;<strong>' +
                        data[i].name + '</strong></a></td><td>' + data[i].email + '</td><td>' +
                        '<a href="/admin/clientes/ativar/' + data[i].id +
                        '" class="text-success" title="Ativar" onclick="return confirm(\'Confirmar o ativamento de ' +
                        data[i].name + '?\');"><i class="mdi mdi-check"></i></a></td></tr>';
                    $('#desativados').append(linha);
                }
                $('#total').html(data.length);
                $("#perfil>a").click(function (e) {
                    e.preventDefault();
                    carregaPerfil($(this).attr('perfil'));
                })
            });
        }

        function carregaControles() {
            $.getJSON('/admin/clientes/controle', function (data) {
                $('#clientes-ativos').html(data.ativos);
                $('#ultimo-cliente').html(data.ultimoCliente);
                $('#clientes-desativados').html(data.desativados);
                $('#ultimo-desativado').html(data.ultimoDesativado);
                $('#sem-endereco').html(data.semEndereco);
                $('#ultimo-sem-endereco').html(data.ultimoSemEndereco);
                $('#sem-foto').html(data.semFoto);
                $('#ultimo-sem-foto').html(data.ultimoSemFoto);
                $('#total-clientes').html(data.ativos + data.desativados);
            });
        }

        function carregaPerfil(id) {
            $.getJSON('/admin/clientes/perfil', { id }, function (data) {
                $('#imagem-perfil').html('<img src="/storage/' + data[0].foto + '" class="rounded-circle" width="150" height="150" />');
                $('#nome-perfil').html(data[0].name);
                $('#tipo-perfil').html(data[0].perfil);
                $('#pontos-perfil').html(data[0].pontos);
                $('#email-perfil').html(data[0].email);
                $('#celular1-perfil').html(data[0].contato.celular1);
                $('#celular2-perfil').html(data[0].contato.celular2);
                $('#residencial-perfil').html(data[0].contato.residencial);
                $('#cep-perfil').html(data[0].contato.cep);
                endereco = data[0].contato.rua != "—" ? data[0].contato.rua + ", " + data[0].contato.numero : data[0].contato.rua;
                complemento = data[0].contato.complemento == "—" ? " " : " — " + data[0].contato.complemento;
                $('#endereco-perfil').html(endereco + '' + complemento);
                bairro = data[0].contato.bairro == "—" ? "—" : data[0].contato.bairro + ', ' + data[0].contato.cidade + ' (' + data[0].contato.uf + ')';
                $('#bairro-perfil').html(bairro);
                $('#facebook-perfil').html(data[0].contato.facebook == "—" ? "" : '<a href="' + data[0].contato.facebook +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook"></i></a>');
                $('#twitter-perfil').html(data[0].contato.twitter == "—" ? "" : '<a href="' + data[0].contato.twitter +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a>');
                $('#instagram-perfil').html(data[0].contato.instagram == "—" ? "" : '<a href="' + data[0].contato.instagram +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fab fa-instagram"></i></a>');
                $('#editar').html('<a href="/admin/clientes/editar/' + id + '" class="btn btn-primary btn-sm">EDITAR</a>');                
                $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA53FKE3TYOq5c2127s6TkcilVngyfFves&address=' +
                    data[0].contato.bairro + " " + data[0].contato.cep, dataType: 'json',
                    success: function (data) {
                        if (data.results.length > 0) {
                            var uluru = {
                                lat: parseFloat(data.results[0].geometry.location.lat),
                                lng: parseFloat(data.results[0].geometry.location.lng)
                            };
                            var map = new google.maps.Map(document.getElementById('map'), { zoom: 15, center: uluru });
                            var marker = new google.maps.Marker({ position: uluru, map: map });
                        } else {
                            var uluru = {lat: -31.7726923, lng: -52.3453726};
                            var map = new google.maps.Map(document.getElementById('map'), { zoom: 15, center: uluru });
                            var marker = new google.maps.Marker({ position: uluru, map: map });
                        }
                    }
                });
                $("#ExemploModalCentralizado").modal('show');
            });
        }

        $(function () {
            if (filtro == 'za') {
                carregarZA(1);
            } else {
                carregarAZ(1);
            }
            carregaDesativados();
            carregaControles();
        });

        $('#za').on('click', function () { carregarZA(1); });
        $('#az').on('click', function () { carregarAZ(1); });

    </script>
    @endsection