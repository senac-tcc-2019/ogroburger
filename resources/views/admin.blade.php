@extends('layouts.dashboard', ["current" => "admin"])

@section('conteudo')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <!-- Bread crumb and right sidebar toggle -->
            <div class="col-md-5">
                <h4 class="page-title">DASHBOARD</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Home</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-md-7">
                <div class="text-right upgrade-btn">
                    <a href="/admin/novo-pedido" class="btn btn-danger text-white">
                        <i class="fas fa-plus-square"></i> NOVO PEDIDO</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex">
                            <div>
                                <h4 class="card-title">PEDIDOS</h4>
                                <h5 class="card-subtitle subtitulo"></h5>
                            </div>
                            <div class="ml-auto d-flex no-block align-items-center">
                                <ul class="list-inline font-12 dl m-r-5 m-b-3">                                    
                                    <li class="list-inline-item"><i class="mdi mdi-cart-outline text-primary">
                                        </i> PRODUTO(S) DO PEDIDO</li>
                                    <li class="list-inline-item"><i class="mdi mdi-pencil text-info">
                                        </i> EDITA O CLIENTE</li>
                                </ul>
                            </div>
                        </div>
                        <div class="d-md-flex justify-content-end">
                            <ul class="list-inline m-r-5">
                                <li class="tamanho-input-busca">
                                    <form method="POST" action="/admin/clientes/pesquisar">
                                        @csrf
                                        <div class="input-group stylish-input-group">
                                            <input type="search" class="form-control form-control-sm"
                                                placeholder="PESQUISAR PEDIDO POR ID" name="aPesquisar" id="aPesquisar"
                                                requerid />
                                            <span class="input-group-addon">
                                                <button type="submit" id="pesquisar">
                                                    <i class="mdi mdi-magnify"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                            <div class="dropdown m-r-5">
                                <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    FILTRAR POR
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <h6 class="dropdown-header">TIPO DE CLIENTE</h6>
                                    <button class="dropdown-item" type="button" id="default">DEFAULT</button>
                                    <button class="dropdown-item" type="button" id="cliente">CLIENTE</button>
                                </div>
                            </div>
                            <div class="dropdown m-r-5">
                                <button class="btn btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    GERAR RELATÓRIO
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <h6 class="dropdown-header">VISUALIZAR PDF</h6>
                                    <a class="dropdown-item" href="/admin/clientes/pdf-clientes-ativos"
                                        target="_blank">Clientes ativos</a>
                                    <a class="dropdown-item" href="/admin/clientes/pdf-clientes-desativados"
                                        target="_blank">Clientes desativados</a>
                                    <div class="dropdown-divider"></div>
                                    <h6 class="dropdown-header">BAIXAR PDF</h6>
                                    <a class="dropdown-item"
                                        href="/admin/clientes/pdf-clientes-ativos-download">Clientes ativos</a>
                                    <a class="dropdown-item"
                                        href="/admin/clientes/pdf-clientes-desativados-download">Clientes
                                        desativados</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table v-middle text-nowrap" id="tabela-pedidos">
                            <thead>
                                <tr class="bg-light">
                                    <th class="border-top-0">N°</th>
                                    <th class="border-top-0">CLIENTE</th>
                                    <th class="border-top-0 text-center">REALIZADO</th>
                                    <th class="border-top-0 text-center">PRODUTO(S)</th>
                                    <th class="border-top-0 text-center">TOTAL</th>
                                    <th class="border-top-0 text-center">STATUS</th>
                                    <th class="border-top-0 text-center">AÇÃO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- ITENS DA TBL INSERIDOS DINAMICAMENTE PELO JS -->
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <nav id="paginationNav">
                            <ul class="pagination pagination-sm justify-content-center">
                                <!-- ITENS DA PAGINACAO INSERIDOS DINAMICAMENTE PELO JS -->
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog"
        aria-labelledby="TituloModalCentralizado" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TituloModalCentralizado">PERFIL COMPLETO DO CLIENTE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-auto text-center">
                                <span id="imagem-perfil"></span>
                                <p class="mt-2">
                                    <span id="facebook-perfil"></span>
                                    <span id="twitter-perfil"></span>
                                    <span id="instagram-perfil"></span>
                                </p>
                            </div>
                            <div class="col-md-auto">
                                <h4 class="card-title mb-1" id="nome-perfil"></h4>
                                <h6 class="card-subtitle text-muted mb-0"><em><span id="tipo-perfil"></span></em></h6>
                                <p class="mb-1"><strong>Pontos:</strong> <span id="pontos-perfil"></span></p>
                                <p class="text-sm">
                                    <strong>E-mail:</strong> <span id="email-perfil"></span><br />
                                    <strong>Celular 1:</strong> <span id="celular1-perfil"></span><br />
                                    <strong>Celular 2:</strong> <span id="celular2-perfil"></span><br />
                                    <strong>Tel. residencial:</strong> <span id="residencial-perfil"></span><br />
                                    <strong>CEP:</strong> <span id="cep-perfil"></span><br />
                                    <strong>End.:</strong> <span id="endereco-perfil"></span><br />
                                    <strong>Bairro:</strong> <span id="bairro-perfil"></span><br />
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div id="map" style="height: 190px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span id="editar"></span>
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">FECHAR</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalPedido" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TituloModalCentralizado">EDITAR PEDIDO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <h4 class="card-title" id="pedido-id"></h4>
                    <h6 class="card-subtitle text-muted mb-0" id="pedido-usuario"></h6>
                    <hr>
                    <h6>ALTERAR STATUS DO PEDIDO</h6>
                    <form method="POST" class="form-horizontal form-material" id="frm">
                        @csrf
                        <div class="form-group">
                            <label for="status">Selecione o novo status do pedido</label>
                            <select name="status" class="form-control form-control-line" id="status">
                                <option value="0">Selecionar...</option>
                                <option value="AGUARDANDO">AGUARDANDO</option>
                                <option value="PRONTO">PRONTO</option>
                                <option value="ENVIADO">ENVIADO</option>
                                <option value="RETIRAR">RETIRAR</option>
                            </select>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-success btn-sm btn-espaco">
                               ALTERAR STATUS</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <span id="pedido-encerrar"></span>
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">FECHAR</button>
                </div>
            </div>
        </div>
    </div>  
    <div class="modal fade" id="ModalLanches" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TituloModalCentralizado">LISTA DOS PRODUTOS DO PEDIDO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="card-title" id="lanches-pedido"></h4>
                    <h6 class="card-subtitle text-muted mb-0" id="lanches-usuario"></h6>
                    <hr>
                    <h6>PRODUTOS</h6>
                    <ul class="list-group" id="lanches">

                    </ul>
                </div>
                <div class="modal-footer">                    
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">FECHAR</button>
                </div>
            </div>
        </div>
    </div>  
     @if (session('OK'))
    <div class="alert alerta-sucesso alert-dismissible" role="alert">
        <i class="fas fa-check-circle"></i>{{ session('OK') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @endsection   

    @section('js')
    <script type="text/javascript">

        function getNextItem(data) {
            i = data.current_page + 1;
            if (data.current_page == data.last_page)
                s = '<li class="page-item disabled">';
            else
                s = '<li class="page-item">';
            s += '<a class="page-link" pagina="' + i + '" href="javascript:void(0);">Próximo</a></li>';
            return s;
        }

        function getPreviousItem(data) {
            i = data.current_page - 1;
            if (data.current_page == 1)
                s = '<li class="page-item disabled">';
            else
                s = '<li class="page-item">';
            s += '<a class="page-link" pagina="' + i + '" href="javascript:void(0);">Anterior</a></li>';
            return s;
        }

        function getItem(data, i) {
            if (data.current_page == i)
                s = '<li class="page-item active">';
            else
                s = '<li class="page-item">';
            s += '<a class="page-link" pagina="' + i + '" href="javascript:void(0);">' + i + '</a></li>';
            return s;
        }

        function montarPaginator(data) {
            $("#paginationNav>ul>li").remove();
            $("#paginationNav>ul").append(getPreviousItem(data));
            if ((data.total % 5) > 0) {
                n = parseInt((data.total / 5)) + 1;
            } else {
                n = data.total / 5;
            }
            if (data.current_page - n / 2 <= 1)
                inicio = 1;
            else if (data.last_page - data.current_page < n)
                inicio = data.last_page - n + 1;
            else
                inicio = data.current_page - n / 2;

            fim = inicio + n - 1;

            for (i = inicio; i <= fim; i++) {
                $("#paginationNav>ul").append(getItem(data, i));
            }
            $("#paginationNav>ul").append(getNextItem(data));
        }

        function montarTabela(data) {
            $("#tabela-pedidos>tbody>tr").remove();
            //console.log(data);
            for (i = 0; i < data.data.length; i++) {               
                if (data.data[i].status == 'AGUARDANDO') {
                    status = '<label class="label label-danger">' + data.data[i].status + '</label>';
                } else if (data.data[i].status == 'PRONTO') {
                    status = '<label class="label label-success">' + data.data[i].status + '</label>';
                } else if (data.data[i].status == 'ENVIADO') {
                    status = '<label class="label label-info">' + data.data[i].status + '</label>';
                } else if (data.data[i].status == 'RETIRAR') {
                    status = '<label class="label label-purple">' + data.data[i].status + '</label>';
                }
                hora = data.data[i].created_at.split(' ');
                $("#tabela-pedidos>tbody").append('<tr><td>' + data.data[i].id +
                    '<td id="perfil"><a href="javascript:void(0);" class="text-secondary" perfil="' +
                    data.data[i].usuario[0].id + '"><img src="/storage/' + data.data[i].usuario[0].foto + '" class="rounded-circle" width="40' +
                    '" height="40" />&nbsp;&nbsp;<strong>' + data.data[i].usuario[0].name + '</strong></a></td>' +
                    '<td class="text-center text-muted">' + hora[1].substr(0, 5) + '</td><td class="text-center" id="lanche"><a href="javascript:void(0);" ' +
                    'class="text-primary" lanche="' + data.data[i].id + '"><i class="mdi mdi-cart-outline"></i></a></td>' +
                    '<td class="text-center">' + data.data[i].total.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</td>' +
                    '<td class="text-center">' + status +
                    '</td><td class="text-center" id="pedido"><a href="javascript:void(0);" ' +
                    'class="text-info" pedido="' + data.data[i].id + '" title="Editar Pedido"><i class="mdi mdi-pencil"></i></a></td></tr>');
            }
        }

        function carregarAZ(pagina) {
            $.get('/admin/pedidos', { page: pagina }, function (resp) {
                montarTabela(resp);
                montarPaginator(resp);
                $("#paginationNav>ul>li>a").click(function () {
                    carregarAZ($(this).attr('pagina'));
                })
                $(".subtitulo").html("Exibindo " + resp.data.length + " pedido(s) de " + resp.total);
                $("#perfil>a").click(function (e) {
                    e.preventDefault();
                    carregaPerfil($(this).attr('perfil'));
                })
                $("#pedido>a").click(function (e) {
                    e.preventDefault();
                    carregaPedido($(this).attr('pedido'));
                })
                $("#lanche>a").click(function (e) {
                    e.preventDefault();
                    carregaLanches($(this).attr('lanche'));
                })
            });
        }

        function carregaPedido(id) {
            $.getJSON('/admin/pedido', { id }, function (data) {
                //console.log(data);
                $('#frm').attr("action", "/admin/pedido/editar/" + id);
                $('#pedido-id').html('PEDIDO DE NÚMERO: ' + id);
                $('#pedido-usuario').html('<img src="/storage/' + data[0].usuario[0].foto + 
                '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;' + data[0].usuario[0].name);
                if (data[0].status == 'AGUARDANDO') {
                    $('#status').val("AGUARDANDO");
                }
                if (data[0].status == 'PRONTO') {
                    $('#status').val("PRONTO");
                }
                if (data[0].status == 'ENVIADO') {
                    $('#status').val("ENVIADO");
                }
                if (data[0].status == 'RETIRAR') {
                    $('#status').val("RETIRAR");
                }
                $('#pedido-encerrar').html('<a href="/admin/pedido/encerrar/' + id +
                '" class="btn btn-danger btn-sm" onclick="return confirm(\'Confirmar o encerramento do pedido de ' +
                data[0].usuario[0].name + '?\');">ENCERRAR PEDIDO</a>');
                $("#ModalPedido").modal('show');      
            });         
        }

        function carregaLanches(id) {
            $.getJSON('/admin/pedido/lanches', { id }, function (data) {
              //console.log(data);
              $('#lanches-pedido').html('PEDIDO DE NÚMERO: ' + id);
                $('#lanches-usuario').html('<img src="/storage/' + data[0].usuario[0].foto + 
                '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;' + data[0].usuario[0].name);
                $("#lanches>li").remove();
               // console.log(data[0].lanches.length);
             for (i = 0; i < data[0].lanches.length; i++) {
              //  console.log(data[0].lanches[i].nome);
               $("#lanches").append('<li class="list-group-item d-flex justify-content-between align-items-center">' +
               '<a href="javascript:void(0);" class="text-secondary"><img src="/storage/' + data[0].lanches[i].foto + '" class="img-thumbnail" width="40' +
                '" height="40" />&nbsp;&nbsp;<strong>' + data[0].lanches[i].nome + '</strong></a>' +
              '<span class="badge badge-primary badge-pill">' + '</span></li>');
            }
              $("#ModalLanches").modal('show');     
            });                                    
        }

        function carregaPerfil(id) {
            $.getJSON('/admin/clientes/perfil', { id }, function (data) {
                $('#imagem-perfil').html('<img src="/storage/' + data[0].foto + '" class="rounded-circle" width="150" height="150" />');
                $('#nome-perfil').html(data[0].name);
                $('#tipo-perfil').html(data[0].perfil);
                $('#pontos-perfil').html(data[0].pontos);
                $('#email-perfil').html(data[0].email);
                $('#celular1-perfil').html(data[0].contato.celular1);
                $('#celular2-perfil').html(data[0].contato.celular2);
                $('#residencial-perfil').html(data[0].contato.residencial);
                $('#cep-perfil').html(data[0].contato.cep);
                endereco = data[0].contato.rua != "—" ? data[0].contato.rua + ", " + data[0].contato.numero : data[0].contato.rua;
                complemento = data[0].contato.complemento == "—" ? " " : " — " + data[0].contato.complemento;
                $('#endereco-perfil').html(endereco + '' + complemento);
                bairro = data[0].contato.bairro == "—" ? "—" : data[0].contato.bairro + ', ' + data[0].contato.cidade + ' (' + data[0].contato.uf + ')';
                $('#bairro-perfil').html(bairro);
                $('#facebook-perfil').html(data[0].contato.facebook == "—" ? "" : '<a href="' + data[0].contato.facebook +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook"></i></a>');
                $('#twitter-perfil').html(data[0].contato.twitter == "—" ? "" : '<a href="' + data[0].contato.twitter +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a>');
                $('#instagram-perfil').html(data[0].contato.instagram == "—" ? "" : '<a href="' + data[0].contato.instagram +
                    '" target="_blank" class="btn btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fab fa-instagram"></i></a>');
                $('#editar').html('<a href="/admin/clientes/editar/' + id + '" class="btn btn-primary btn-sm">EDITAR</a>');
                $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA53FKE3TYOq5c2127s6TkcilVngyfFves&address=' +
                        data[0].contato.bairro + " " + data[0].contato.cep, dataType: 'json',
                    success: function (data) {
                        if (data.results.length > 0) {
                            var uluru = {
                                lat: parseFloat(data.results[0].geometry.location.lat),
                                lng: parseFloat(data.results[0].geometry.location.lng)
                            };
                            var map = new google.maps.Map(document.getElementById('map'), { zoom: 15, center: uluru });
                            var marker = new google.maps.Marker({ position: uluru, map: map });
                        } else {
                            var uluru = { lat: -31.7726923, lng: -52.3453726 };
                            var map = new google.maps.Map(document.getElementById('map'), { zoom: 15, center: uluru });
                            var marker = new google.maps.Marker({ position: uluru, map: map });
                        }
                    }
                });
                $("#ExemploModalCentralizado").modal('show');
            });
        }

        $(function () {           
            carregarAZ(1);           
        });

    </script>
    @endsection