<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ogro Burger - Hamburgueria</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon (ícone na aba do navegador) -->
    <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="{!! asset('favicon.ico') !!}" />
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/styles-app.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Anton|Kadwa" rel="stylesheet">
</head>

<body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <img src="{!! asset('img/logo-menu.png') !!}" /> Ogro Burger</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#about">PEDIDO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#projects">HAMBURGUÊS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#signup">CONTATO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger link-destaque"
                            href="{{ route('register') }}">CADASTRE-SE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger link-destaque" href="{{ route('login') }}">LOGIN</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Header -->
    <header class="masthead">
        <div class="container d-flex h-100 align-items-center">
            <div class="mx-auto text-center">
                <h1 class="mx-auto my-0 text-uppercase">OGRO BURGER</h1>
                <h2 class="mx-auto mt-2 mb-5"><em>Para ogros de fome, nada melhor que pedir
                        aquele hambúrguer delicioso.</em>
                </h2>
                <a href="#about" class="btn btn-primary js-scroll-trigger">Realizar
                    pedido <i class="fas fa-angle-down text-white"></i></a>
            </div>
        </div>
    </header>
    <!-- About Section -->
    <section id="about" class="about-section text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <h2 class="text-white mb-4">PEDIDO</h2>
                    <p class="text-white-50">
                        O Ogro Burger conta com um sistema de pedidos que ajuda seus clientes a
                        realizar seus pedidos de forma rápida e eficiente. Com isso, os clientes
                        poderão acompanhar a rota do seu pedido até seu destino, seus pontos
                        adquiridos e muito mais. E tudo isso pode ser acompanhado atraves da nossa seção
                        <strong><a href="/register">pedidos</a></strong>. Para mais informações entre em
                        <strong><a href="#signup" class="js-scroll-trigger">contado</a></strong> conosco.
                    </p>
                </div>
            </div>
            <img src="{!! asset('img/fundo-pedido.png') !!}" class="img-fluid" alt="img-fundo">
        </div>
    </section>
    <!-- Projects Section -->
    <section id="projects" class="projects-section bg-light">
        <div class="container">
            <h4 id="titulo-destaque"><i class="fas fa-star"></i> <strong>DESTAQUE PARA HOJE</strong></h4>
            <div class="data-destaque">
                <small><em>Pelotas-RS, 23 de set. de 2018. Atualizado as 22:13.</em></small>
            </div>
            <!-- Featured Project Row -->
            <div class="row align-items-center no-gutters mb-4 mb-lg-5">
                <div class="col-xl-8 col-lg-7">
                    <img class="img-fluid mb-3 mb-lg-0" src="{!! asset('img/destaque.jpg') !!}" alt="img-lanche">
                </div>
                <div class="col-xl-4 col-lg-5">
                    <div class="featured-text text-lg-left titulo-preto">
                        <h4>ACEBOLADO</h4>
                        <p class="text-black-50 mb-0">
                            <ul>
                                <li>Pão — <em>500g</em></li>
                                <li>Maionese — <em>100g</em></li>
                                <li>Muita cebola rocha — <em>200g</em></li>
                                <li>Alface — <em>100g</em></li>
                                <li>Hambúrguer de carne/frango — <em>300g</em></li>
                            </ul>
                        </p>
                        <h2 class="text-right">R$ 14,00</h2>
                    </div>
                </div>
            </div>
            <h4 class="text-right">🍔<strong>CARDÁPIO</strong></h4>

            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($lanches as $i=>$lanche)                    
                    <div class="carousel-item {{ $i == 0 ? ' active' : '' }}">                    
                        <!-- Project One Row -->
                        <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
                            <div class="col-lg-6">
                                <img class="img-fluid" src="/storage/{{$lanche->foto}}" alt="img-lanche">
                            </div>
                            <div class="col-lg-6">
                                <div class="bg-black h-100 project">
                                    <div class="d-flex h-100">
                                        <div class="project-text w-100 my-auto text-lg-left titulo-branco">
                                            <h4>{{$lanche->nome}}</h4>
                                            <p class="mb-0 text-white-50">
                                                <ul class="text-white-50">
                                                @foreach($lanche->produtos as $p) 
                                                    <li><strong>{{$p->nome}}</strong> — <em>{{$p->pivot->quantidadeproduto}} gramas</em></li>                                                   
                                                @endforeach
                                                </ul>
                                            </p>
                                            <h2 class="text-right">{{'R$ '.number_format($lanche->preco, 2, ',', '.')}}</h2>
                                            <hr class="d-none d-lg-block mb-0 ml-0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
    <div class="container">
        <h4 class="text-center titulo-saiba-mais">
            <i class="fas fa-bookmark"></i> <strong>SAIBA MAIS</strong></h4>
        <br>
        <div class="row justify-content-center mais-infos">
            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <img src="{!! asset('img/promocoes.jpg') !!}" class="img-fluid" alt="img-promocoes">
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <img src="{!! asset('img/monte.png') !!}" class="img-fluid" alt="img-monte-o-seu">
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <img src="{!! asset('img/completo.png') !!}" class="img-fluid" alt="img-cardapio-completo">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Signup Section -->
    <section id="signup" class="signup-section">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto text-center">
                    <i class="fas fa-paper-plane fa-2x mb-2 text-white"></i>
                    <h2 class="text-white mb-5">Inscreva-se para receber atualizações!</h2>
                    <form class="form-inline d-flex">
                        <input type="email" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" id="inputEmail"
                            placeholder="Seu endereço de e-mail...">
                        <button type="submit" class="btn btn-primary mx-auto">Inscreva-se</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section -->
    <section class="contact-section bg-black">
        <div class="container">
            <div class="map-home mt-5">
                <h2 class="text-white mb-5">
                    <i class="fas fa-map-marker-alt"></i> ONDE ESTAMOS</h2>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3391.8266487962583!2d-52.331871385308844!3d-31.775213681317265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9511b5c6261c2f25%3A0xa5551db89e6f4c7d!2sR.+Tiradentes%2C+1381+-+Porto%2C+Pelotas+-+RS%2C+96010-160!5e0!3m2!1spt-BR!2sbr!4v1539143298028"
                    width="80%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="social d-flex justify-content-center">
                <a href="#" class="mx-2">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="#" class="mx-2">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#" class="mx-2">
                    <i class="fab fa-instagram"></i>
                </a>
            </div>
        </div>
    </section>
    <!-- Footer -->
    <footer class="bg-black text-center text-white-50">
        <div class="container">
            <small>COPYRIGHT &copy; <a href="#page-top" class="js-scroll-trigger">OGRO BURGER</a>
                2018 — ALGUNS DIREITOS RESERVADOS!<br /> BY
                <a href="https://gitlab.com/rafaelcalearo" target="_blank">ЯTO</a>
                — TRABALHO FEITO COM <i class="fas fa-heart"></i></small>
        </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Plugin JavaScript -->
    <script src="{{ asset('js/jquery.easing.min.js') }}"></script>
    <!-- Custom scripts for this template -->
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/jquery.flexisel.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#flexiselDemo3").flexisel({
                visibleItems: 3,
                itemsToScroll: 1,
                autoPlay: {
                    enable: true,
                    interval: 5000,
                    pauseOnHover: true
                }
            });
        });
    </script>
</body>

</html>