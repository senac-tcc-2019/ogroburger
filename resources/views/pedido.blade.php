@extends('layouts.dashboard', ["current" => "admin"])
@section('conteudo')
<div class="page-wrapper">
    <!-- PARTE DO CONTEUDO EM SI -->
    <div class="page-breadcrumb">
        <!-- BARRA DE CAMINHO (ONDE ESTOU!)" -->
        <div class="row align-items-center">
            <div class="col-12">
                <h4 class="page-title">PEDIDO</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="\admin">Home (Dashboard)</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Novo pedido</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div><!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) -->
    <div class="container-fluid">
        <!-- CONTEUDO FLUIDO  -->
        <div class="row">
            <!-- LINHA -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <!-- COLUNA DO FORM DE CRIACAO DE UMA NOVA COMPRA -->
                <div class="card">
                    <div class="card-header titulo-card">
                        <h5>NOVO PEDIDO</h5>
                        <div class="form-requerido">
                            <small><em>* O CAMPO É OBRIGATÓRIO!</em></small>
                        </div>
                    </div>
                    <div class="card-body">
                        <h6 class="mt-4 mb-2 text-muted">PEDIDO DE N.° {{ $proximo_pedido }}</h6>
                        <form action="/admin/pedido/new" method="POST" class="form-horizontal form-material">
                            @csrf
                            <h6 class="card-title"><strong>INFORMAÇÕES DO PEDIDO</strong></h6>
                            <div class="borda">
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="clientes"><strong>Pesquisar cliente</strong></label>
                                            <input type="input" name="clientes" placeholder="Pesquisar cliente"
                                                class="form-control" id="clientes" />                                         
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="cliente"><strong>Cliente</strong></label>
                                            <input type="text" name="cliente" class="form-control" id="cliente" />
                                            <input type="hidden" id="idcliente" name="idcliente" value="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="status"><strong>Status</strong></label>
                                            <select name="status" class="form-control">
                                                <option value="AGUARDANDO">Selecionar...</option>
                                                <option>AGUARDANDO</option>
                                                <option>PRONTO</option>
                                                <option>ENVIADO</option>
                                                <option>RETIRAR</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="data-pedido"><strong>Data do pedido</strong></label>
                                            <input type="text" name="data" value="{{ date('d/m/Y') }}"
                                                class="form-control{{ $errors->has('data') ? ' is-invalid' : '' }}"
                                                id="data-pedido" />
                                            @if ($errors->has('data'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('data') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6 class="card-title mt-4"><strong>PRODUTO(S) DO PEDIDO</strong></h6>
                            <div class="borda">
                                <div class="form-row">
                                    <div class="form-group col-md-8">
                                        <label for="produtos"><strong>Pesquisar produto</strong></label>
                                        <input type="input" name="produtos" placeholder="Pesquisar produto"
                                            class="form-control" id="produtos" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label><strong>Ação</strong></label>
                                        <button type="button" disabled="disabled" class="btn btn-info btn-block"
                                            id="adicionar">ADICIONAR PRODUTO</button>
                                    </div>
                                </div>
                                <div id="quadro">
                                    <div class="form-row" id="linha0">
                                        <div class="form-group col-md-3">
                                            <label for="produto0"><strong>Produto</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" name="produto0" class="form-control" id="produto0" />
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="quantidade0"><strong>Quantidade</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" name="quantidade0" placeholder="Ex.: 2"
                                                class="form-control quantidade" id="quantidade0" />
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="custo0"><strong>Valor</strong>
                                                <span class="form-requerido">*</span>
                                                <!-- VER A CLASS "aviso-senha"! -->
                                                <span class="aviso-senha">· 0.00</span></label>
                                            <input type="text" name="custo0" placeholder="Ex.: 1.99"
                                                class="form-control" id="custo0" />
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="total0"><strong>Total</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" name="total0" value="0" readonly="readonly"
                                                class="form-control" id="total0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-4">
                                <h6><strong>QTD. TOTAL DE PRODUTOS:</strong> <span id="totalProdutos">0</span></h6>
                                <h4><strong>TOTAL DA COMPRA:</strong> <span id="totalCompra">R$ 0,00</span></h4>
                                <input type="hidden" id="totalfinal" name="totalfinal" value="">
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="submit" disabled="disabled" class="btn btn-success btn-sm btn-espaco"
                                    id="cadastrar"> CADASTRAR PEDIDO</button>
                                <a href="/admin/novo-pedido" class="btn btn-secondary btn-sm btn-espaco">CANCELAR</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->
        </div><!-- FIM DA LINHA -->
    </div><!-- FIM DO CONTEUDO FLUIDO  -->
    <!-- AQUI TERIA Q TER UMA </div> P/ FECHAR A PARTE DO CONTEUDO EM SI, MAS ELA ESTA NO LAYOUT DO DASHBOARD -->
    @endsection
    @if (session('OK'))
    <div class="alert alerta-sucesso alert-dismissible" role="alert">
        <i class="fas fa-check-circle"></i>{{ session('OK') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @section('js')
    <script type="text/javascript">
        $(document).ready(function () {

            $('#clientes').keyup(function () {
                var p = $('#clientes').val();
                $.getJSON('/admin/clientes/procurar', { p }, function (data) {                    
                    $('#cliente').val(data[0].name);
                    $('#idcliente').val(data[0].id);
                });
            });

            var i = 0;
            var totais = [];       
            var quantidades = [];       
            
            $('#produtos').keyup(function () {
              var p = $('#produtos').val();            
               $.getJSON('/admin/lanche/procurar', { p }, function (data) {                    
                   $('#produto' + i).val(data[0].nome);
                  $('#custo' + i).val(data[0].preco.toFixed(2));
                });
            });

            $('#quantidade' + i).keyup(function () {
                $(this).val(this.value.replace(/\D/g, ''));
            });

            $('#custo' + i).keyup(function () {
                if ($('#custo' + i).val().split(".")) {
                    var parte = $('#custo' + i).val().split(".");
                    var valor = parte[0] + "." + parte[1];
                    quantidades[i] = parseInt($('#quantidade' + i).val());
                    totais[i] = valor * quantidades[i];
                    if (totais[i] > 0 || totais[i] > 0.00) {
                        $('#total' + i).val(totais[i].toFixed(2).replace('.', '.'));
                        $('#totalProdutos').html(quantidades[i]);
                        $('#totalCompra').html('R$ ' + totais[i].toFixed(2).replace('.', ','));
                        $('#totalfinal').val(totais[i]);
                        $('#adicionar').add('#cadastrar').attr("disabled", false);
                    } else {
                        $('#adicionar').add('#cadastrar').attr("disabled", true);
                        $('#total' + i).val(0);
                    }
                }
            });

            $('#adicionar').click(function (e) {
                e.preventDefault();               
                $('#adicionar').add('#cadastrar').attr("disabled", true);
                $("#produto" + i).css("background", "#e9ecef", "pointer-events", "none", "touch-action", "none");
                $('#produto' + i + ' option:not(:selected)').prop('disabled', true);
                $('#quantidade' + i).add('#custo' + i).prop("readonly", true);
                i++;                          
                $("#quadro").append('<div class="form-row" id="linha' + i + '"><div class="form-group ' +
                    'col-md-3"><label for="produto' + i + '"><strong>Produto</strong> <span class="for' +
                    'm-requerido">*</span></label><input type="text" name="produto' + i + '" class="for' +
                    'm-control" id="produto' + i + '" /></div><div class="form-group col-md-2"><label ' +
                    'for="quantidade' + i + '"><strong>Quantidade</strong> <span class="form-requerido' +
                    '">*</span></label><input type="number" name="quantidade' + i + '" placeholder="Ex' +
                    '.: 2" class="form-control" id="quantidade' + i + '" /></div><div class="form-grou' +
                    'p col-md-3"><label for="custo' + i + '"><strong>Valor</strong> <span class="f' +
                    'orm-requerido">*</span><span class="aviso-senha">· 0.00</span></label><input type' +
                    '="text" name="custo' + i + '" placeholder="Ex.: 1.99" class="form-control" id="cu' +
                    'sto' + i + '"/></div><div class="form-group col-md-2"><label for="total' + i + '"' +
                    '><strong>Total</strong> <span class="form-requerido">*</span></label><input type=' +
                    '"text" name="total' + i + '" readonly="readonly" value="0" class="form-control" i' +
                    'd="total' + i + '" /></div><div class="form-group col-md-2"><label><strong>Ação</' +
                    'strong></label><div><button type="button" disabled="disabled" class="btn btn-dang' +
                    'er btn-block" id="remover' + i + '">REMOVER</button></div></div>');

                $('#remover' + i).attr("disabled", false);

                totais[i] = 0;
                quantidades[i] = 0;

             //   $('#produtos' + i).keyup(function () {
              //  var p = $('#produtos').val();
               // $.getJSON('/admin/lanche/procurar', { p }, function (data) {                    
               //     $('#produto' + i).val(data[0].nome);
               //     $('#custo' + i).val(data[0].preco);
             //   });
      //  }); 
      $('#produtos').keyup(function () {
              var p = $('#produtos').val();
              //console.log(p);
               $.getJSON('/admin/lanche/procurar', { p }, function (data) {                    
                   $('#produto' + i).val(data[0].nome);
                  $('#custo' + i).val(data[0].preco.toFixed(2));
                });
            });

                $('#quantidade' + i).keyup(function () {
                    $(this).val(this.value.replace(/\D/g, ''));
                });

                $('#custo' + i).keyup(function () {
                    if ($('#custo' + i).val().split(".")) {
                        var parte = $('#custo' + i).val().split(".");
                        var valor = parte[0] + "." + parte[1];
                        quantidades[i] = parseInt($('#quantidade' + i).val());
                        totais[i] = valor * quantidades[i];
                        //console.log(quantidades);
                        //console.log(totais);
                        if (totais[i] > 0 || totais[i] > 0.00) {
                            $('#total' + i).val(totais[i].toFixed(2).replace('.', '.'));                            
                            var totalCompra = 0;
                            var quantProdutos = 0;
                            for (var y = 0; y < totais.length; y++) {
                                totalCompra += totais[y];
                            }
                            for (var z = 0; z < quantidades.length; z++) {
                                quantProdutos += quantidades[z];
                            }
                            //console.log(quantProdutos);
                            $('#totalProdutos').html(quantProdutos);
                            $('#totalCompra').html('R$ ' + totalCompra.toFixed(2).replace('.', ','));
                            $('#totalfinal').val(totalCompra);
                            $('#adicionar').add('#cadastrar').attr("disabled", false);
                            $('#remover' + i).attr("disabled", true);
                        } else {
                            $('#adicionar').add('#cadastrar').attr("disabled", true);
                            $('#total' + i).val(0);
                            $('#remover' + i).attr("disabled", false);
                        }
                    }
                });

                $('#remover' + i).click(function (e) {
                    e.preventDefault();
                    $('#linha' + i).remove();
                    totais.pop();
                    quantidades.pop();
                    var resto = 0;
                    var restoQuantidade = 0;
                    for (var z = 0; z < i; z++) {
                        resto += totais[z];
                    }
                    for (var w = 0; w < i; w++) {
                        restoQuantidade += quantidades[w];
                    }
                    $('#totalProdutos').html(restoQuantidade);
                    $('#totalCompra').html('R$ ' + resto.toFixed(2).replace('.', ','));
                    i = i - 1;
                    $('#adicionar').add('#cadastrar').attr("disabled", false);
                });
            });
        });   
    </script>
    @endsection