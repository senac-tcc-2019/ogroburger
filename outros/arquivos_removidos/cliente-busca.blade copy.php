@extends('layouts.dashboard', ["current" => "clientes"])
@section('conteudo')
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-md-5">
         <h4 class="page-title">CLIENTES</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/admin">Home (Dashboard)</a></li>
                  <li class="breadcrumb-item"><a href="/admin/clientes">Clientes</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Resultado da pesquisa</li>
               </ol>
            </nav>
         </div>
      </div>
      <div class="col-md-7">
         <div class="text-right upgrade-btn">
            <a href="/admin/clientes/novo" class="btn btn-success text-white"><i class="fa fa-plus-square"></i> NOVO CLIENTE</a>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="card">
            <input type="hidden" value="{{ $palavra }}" id="palavra" />
            <div class="card-body">
               <div class="d-md-flex">
                  <div>
                     <h4 class="card-title">RESULTADO DA PESQUISA</h4>
                     <h5 class="card-subtitle">Sua pesquisa por
                        "<strong><span id="pal"></span></strong>" retornou o total
                        de <span id="total"></span> cliente(s).
                     </h5>
                  </div>
                  <div class="ml-auto d-flex no-block align-items-center">
                     <ul class="list-inline font-12 dl m-r-5 m-b-3">
                        <li class="list-inline-item"><i class="mdi mdi-pencil text-info"></i> EDIÇÃO DOS DADOS DO CLIENTE</li>
                        <li class="list-inline-item"><i class="mdi mdi-block-helper text-danger"></i> DESATIVA O CLIENTE</li>
                     </ul>
                  </div>
               </div>
               <div class="d-md-flex justify-content-end">
                  <ul class="list-inline m-r-5 m-b-0">
                     <li class="tamanho-input-busca">
                        <div class="input-group stylish-input-group">
                           <input type="search" class="form-control form-control-sm"
                              placeholder="PESQUISAR CLIENTE" name="aPesquisar"
                              id="aPesquisar" requerid />
                           <span class="input-group-addon">
                           <button type="submit" id="pesquisar">
                           <i class="mdi mdi-magnify"></i>
                           </button>
                           </span>
                        </div>
                     </li>
                     <li></li>
                  </ul>
               </div>
            </div>
            <div class="table-responsive tamanho-tbl">
               <table class="table v-middle text-nowrap">
                  <thead>
                     <tr class="bg-light">
                        <th class="border-top-0">CLIENTE</th>
                        <th class="border-top-0">E-MAIL</th>
                        <th class="border-top-0">ENDEREÇO</th>
                        <th class="border-top-0 text-center">BAIRRO</th>
                        <th class="border-top-0 text-center">CELULAR</th>
                        <th class="border-top-0 text-center">AÇÕES</th>
                     </tr>
                  </thead>
                  <tbody id="linha">
                  </tbody>
               </table>
               <div class="text-center mt-5 aviso-tabela"></div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function () {
    var p = $('#palavra').val();
    $.getJSON('/admin/clientes/procurar', {p}, function (data) {
        $('#total').html(data.length);
        $('#pal').html(p);
        $('#aPesquisar').val(p);
        if (data.length <= 0) {
            $('.aviso-tabela').append('<h3 class="card-title">NENHUM RESULTADO ENCONTRADO.</h3>' +
                '<h5 class="card-subtitle">Sua pesquisa por "<strong>' + p + '</strong>" não re' +
                'tornou nada.</h5><h5 class="card-subtitle">Use outros termos!</h5>');
        } else {
            for (i = 0; i < data.length; i++) {
                var part1 = '<tr><td><a href="" class="text-secondary"><img src="/storage/' +
                    data[i].foto + '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;<strong>' +
                    data[i].name + '</strong></a></td><td>' +
                    data[i].email + '</td>';
                var rua = data[i].contato.rua != "—" ? data[i].contato.rua + ", n ° " + data[i].contato.numero : "—";
                var complemento = data[i].contato.complemento != "—" ? " — " + data[i].contato.complemento : "";
                var part2 = '<td>' + rua + '' + complemento +
                    '<td class="text-center">' +
                    data[i].contato.bairro + '</td><td class="text-center">' +
                    data[i].contato.celular1 + '</td><td class="text-center"><a href="/admin/clientes/editar/' +
                    data[i].id + '" class="text-info" title="Editar"><i class="mdi mdi-pencil"></i></a>&nbsp;<a href="/admin/desativar/' +
                    data[i].id + '" class="text-danger" title="Desativar"><i class="mdi mdi-block-helper"></i></a></td></tr>';
                $('#linha').append(part1 + '' + part2);
            }
        }
    });
    // window.history.pushState("", "", url);
});

$(function () {
    $('body').on('click', '#pesquisar', function (e) {
        e.preventDefault();
        var p = $('#aPesquisar').val();
        $('#palavra').val(p);
        $.getJSON('/admin/clientes/procurar', {p}, function (data) {
            $('#total').html(data.length);
            $('#pal').html(p);
            if (data.length <= 0) {
                $('#linha>tr').remove();
                $('.aviso-tabela').append('<h3 class="card-title">NENHUM RESULTADO ENCONTRADO.</h3>' +
                    '<h5 class="card-subtitle">Sua busca por "<strong>' + p + '</strong>" não retornou nada.</h5>' +
                    '<h5 class="card-subtitle">Use outros termos!</h5>');
            } else {
                $('.aviso-tabela').remove();
                $('#linha>tr').remove();
                for (i = 0; i < data.length; i++) {
                    var part1 = '<tr><td><a href="" class="text-secondary"><img src="/storage/' +
                        data[i].foto + '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;<strong>' +
                        data[i].name + '</strong></a></td><td>' + data[i].email + '</td>';
                    var rua = data[i].contato.rua != "—" ? data[i].contato.rua + ", n ° " + data[i].contato.numero : "—";
                    var complemento = data[i].contato.complemento != "—" ? " — " + data[i].contato.complemento : "";
                    var part2 = '<td>' + rua + '' + complemento +
                        '<td class="text-center">' + data[i].contato.bairro + '</td>' +
                        '<td class="text-center">' + data[i].contato.celular1 + '</td>' +
                        '<td class="text-center">' +
                        '<a href="/admin/clientes/editar/' + data[i].id + '" class="text-info"' +
                        'title="Editar"><i class="mdi mdi-pencil"></i></a>&nbsp;' +
                        '<a href="/admin/desativar/' + data[i].id + '" class="text-danger" title="Desativar"><i class="mdi mdi-block-helper"></i></a>' +
                        '</td>' +
                        '</tr>';
                    $('#linha').append(part1 + '' + part2);
                }
            }
        });
        //window.history.pushState("", "", url);
    });
});
</script>
@endsection
