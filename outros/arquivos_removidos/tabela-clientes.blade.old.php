<div class="card-body">
   <div class="d-md-flex">
      <div>
         <h4 class="card-title">CLIENTES ATIVOS</h4>
         <h5 class="card-subtitle">Exebindo {{$clientes->count()}} 
            cliente(s) de {{$clientes->total()}} 
            ({{$clientes->firstItem()}} a {{$clientes->lastItem()}}).
         </h5>
      </div>
      <div class="ml-auto d-flex no-block align-items-center">
         <ul class="list-inline font-12 dl m-r-5 m-b-3">
            <li class="list-inline-item"><i class="mdi mdi-pencil text-info"></i> EDIÇÃO DOS DADOS DO CLIENTE</li>
            <li class="list-inline-item"><i class="mdi mdi-block-helper text-danger"></i> DESATIVA O CLIENTE</li>
         </ul>
      </div>
   </div>
   <div class="d-md-flex justify-content-end">

   <div class="dropdown">
  <button class="btn btn-link btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    GERAR RELATÓRIO
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="/admin/clientes/pdf">PDF</a>
    <a class="dropdown-item" href="/admin/clientes/download">DOWNLOAD</a>
  </div>
</div>

      <ul class="list-inline m-r-5 m-b-0">       
         <li class="tamanho-input-busca">
            <form method="POST" action="/admin/clientes/busca">
            @csrf
            <div class="input-group stylish-input-group">
               <input type="search" class="form-control form-control-sm"  
                  placeholder="PESQUISAR CLIENTE" name="aPesquisar" 
                  id="aPesquisar" requerid />
               <span class="input-group-addon">
               <button type="submit" id="pesquisar">
               <i class="mdi mdi-magnify"></i>
               </button>  
               </span>
            </div>
            </form>
         </li>         
      </ul>
   </div>
</div>
<div class="table-responsive">
   <table class="table v-middle text-nowrap">
      <thead>
         <tr class="bg-light">
            <th class="border-top-0">
            <div class="dropdown">
  <button class="btn btn-link btn-sm dropdown-toggle text-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    CLIENTE
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  <h6 class="dropdown-header">ORDEM</h6>
    <a class="dropdown-item" href="/admin/clientes/pdf">A–Z</a>
    <a class="dropdown-item" href="#" id="za">Z–A</a>
  </div>
</div>
            </th>
            <th class="border-top-0">E-MAIL</th>
            <th class="border-top-0">ENDEREÇO</th>
            <th class="border-top-0 text-center">BAIRRO</th>
            <th class="border-top-0 text-center">CELULAR</th>
            <th class="border-top-0 text-center">AÇÕES</th>
         </tr>
      </thead>
      <tbody>
         @foreach($clientes as $c)
         <tr>
            <td><a href="/admin/clientes/perfil/{{$c->id}}" class="text-secondary">
               <img src="/storage/{{$c->foto}}" class="rounded-circle" width="40" height="40" />
               &nbsp;<strong>{{$c->name}}</strong></a>
            </td>
            <td>{{$c->email}}</td>
            <td>
               @if($c->contato->rua != "—")
               {{$c->contato->rua}}, n° {{$c->contato->numero}}
               @else
               —
               @endif                                      
               @if($c->contato->complemento != "—")
               — {{$c->contato->complemento}}
               @else                                            
               @endif
            </td>
            <td class="text-center">{{$c->contato->bairro}}</td>
            <td class="text-center">{{$c->contato->celular1}}</td>
            <td class="text-center">            
               <a href="/admin/clientes/editar/{{$c->id}}" class="text-info" 
                  title="Editar"><i class="mdi mdi-pencil"></i></a>&nbsp;                                          
               <a href="/admin/clientes/desativar/{{$c->id}}" class="text-danger" title="Desativar"><i class="mdi mdi-block-helper"></i></a>                                           
            </td>
         </tr>
         @endforeach  
      </tbody>
   </table>
</div>
<div class="paginacao">
   {{$clientes->links()}}
</div>