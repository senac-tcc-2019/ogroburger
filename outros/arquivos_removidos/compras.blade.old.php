@extends('layouts.dashboard', ["current" => "compras"])
@section('conteudo')
<div class="page-wrapper">
    <!-- PARTE DO CONTEUDO EM SI -->
    <div class="page-breadcrumb">
        <!-- BARRA DE CAMINHO (ONDE ESTOU!) E BTN DE "NOVO AVISO" -->
        <div class="row align-items-center">
            <div class="col-12">
                <h4 class="page-title">COMPRA</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="\admin">Home (Dashboard)</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Compra</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div><!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
    <div class="container-fluid"><!-- CONTEUDO FLUIDO  -->        
        <div class="row">
            <!-- LINHA -->
            <div class="col-lg-12 col-xlg-12 col-md-12">
                <!-- COLUNA DO FORM DE CRIACAO DE UM NOVO CLIENTE -->
                <div class="card">
                    <div class="card-header titulo-card">
                        <h5>CADASTRO DE NOVA COMPRA</h5>
                        <div class="form-requerido">
                            <small><em>* O CAMPO É OBRIGATÓRIO!</em></small>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="/admin/compras/nova" class="form-horizontal form-material">
                            @csrf
                            <h6 class="card-title mt-4"><strong>INFORMAÇÕES DA COMPRA</strong></h6>
                            <div class="borda">
                                <div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="fornecedor"><strong>Fornecedor</strong>
                                                <span class="form-requerido">*</span></label>
                                            <select class="form-control" name="fornecedor">
                                                @foreach($fornecedores as $f)
                                                <option value="{{$f->id}}">{{$f->razaosocial}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="nf"><strong>N.º do cupom/nota fiscal</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input id="nf" type="text"
                                                class="form-control{{ $errors->has('nf') ? ' is-invalid' : '' }}"
                                                name="nf" placeholder="Ex.: 018931" />
                                            @if ($errors->has('nf'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('nf') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="data"><strong>Data da compra</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="date" id="data"
                                                class="form-control{{ $errors->has('data') ? ' is-invalid' : '' }}"
                                                name="data" value="{{old('data')}}" />
                                            @if ($errors->has('data'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('data') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6 class="card-title mt-4"><strong>PRODUTO(S) DA COMPRA</strong></h6>
                            <div class="borda">
                                <div class="col-md-12 text-right mb-2">
                                    <button type="button" id="adicionar" class="btn btn-info btn-sm">
                                        ADICIONAR PRODUTO</button>
                                    <button type="button" id="calcular" class="btn btn-primary btn-sm">CALCULAR</button>
                                </div>
                                <div id="quadro">
                                    <div class="form-row" id="linha0">
                                        <div class="form-group col-md-3">
                                            <label for="produto0"><strong>Produto</strong>
                                                <span class="form-requerido">*</span></label>
                                            <select class="form-control" name="produto0">
                                                @foreach($produtos as $p)
                                                <option value="{{$p->id}}">{{$p->nome}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="quantidade0"><strong>Quantidade</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" id="quantidade0" class="form-control quantidade"
                                                name="quantidade0" placeholder="Ex.: 2" />
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="custo0"><strong>Custo un.</strong>
                                                <span class="form-requerido">*</span>
                                                <span class="aviso-senha">· 0.00</span></label>
                                            <input type="text" id="custo0" class="form-control" name="custo0"
                                                placeholder="Ex.: 1.99" />
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="total0"><strong>Total</strong>
                                                <span class="form-requerido">*</span></label>
                                            <input type="text" id="total0" class="form-control" name="total0"
                                                readonly="readonly" value="0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-4">
                                <h6><strong>QTD. TOTAL DE PRODUTOS:</strong> <span id="totalItens">0</span></h6>
                                <h4><strong>TOTAL DA COMPRA:</strong> <span id="totalCompra">R$ 0,00</span></h4>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button class="btn btn-success btn-sm btn-espaco" type="submit" id="cadastrar"
                                    disabled="disabled">
                                    <i class="mdi mdi-content-save"></i> CADASTRAR COMPRA</button>
                                <a href="/admin/compras" class="btn btn-secondary btn-sm btn-espaco">
                                    <i class="mdi mdi-close"></i> CANCELAR</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->
        </div><!-- FIM DA LINHA -->
    </div><!-- FIM DO CONTEUDO FLUIDO  -->
    <!-- AQUI TERIA Q TER UMA </div> P/ FECHAR A PARTE DO CONTEUDO EM SI, MAS ELA ESTA NO LAYOUT DO DASHBOARD -->
    @endsection
    @if (session('OK'))
    <div class="alert alerta-sucesso alert-dismissible" role="alert">
        <i class="fas fa-check-circle"></i>{{ session('OK') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @section('js')
    <script type="text/javascript">
        $(document).ready(function () {

            var i = 0;
            var totalParcial = 0;
            var totais = [];
            var linha = '#linha' + i;
            var quantidade = '#quantidade' + i;
            var custo = '#custo' + i;
            var total = '#total' + i;
            var remover = '#remover' + i;
            
            $(quantidade).keyup(function () {
                $(this).val(this.value.replace(/\D/g, ''));
            });

            $(custo).blur(function () {
                if ($(custo).val().split(".")) {
                    var parte = $(custo).val().split(".");
                    var valor = parte[0] + "." + parte[1];
                    totalParcial = valor * $(quantidade).val();
                    if (totalParcial > 0 || totalParcial > 0.00) {
                        $(total).val(totalParcial.toFixed(2).replace('.', '.'));
                    } else {
                        totalParcial = 0;
                        $(total).val(0);
                    }
                }
            });

            $('#adicionar').click(function (e) {
                e.preventDefault();
                if (totalParcial > 0 || totalParcial > 0.00) {
                    totais.push(totalParcial);
                } else {
                    return
                }
                $(quantidade).add(custo).prop("readonly", true);
                i++;
                $("#quadro").append('<div class="form-row" id="linha' + i + '"><div class="form-group col-md-3">' +
                    '<label for="produto"><strong>Produto</strong> <span class="form-requerido">*</span>' +
                    '</label><select name="produto' + i + '" class="form-control">@foreach($produtos as $p)' +
                    '<option value="{{$p->id}}">{{$p->nome}}</option>@endforeach</select></div>' +
                    '<div class="form-group col-md-2"><label for="quantidade"><strong>Quantidade</strong> ' +
                    '<span class="form-requerido">*</span></label>' +
                    '<input id="quantidade' + i + '" type="number" class="form-control" placeholder="Ex.: 2"' +
                    ' name="quantidade' + i + '" /></div><div class="form-group col-md-3">' +
                    '<label for="custo"><strong>Custo un.</strong> <span class="form-requerido">*</span>' +
                    '<span class="aviso-senha">· 0.00</span></label><input id="custo' + i +
                    '" type="text" class="form-control" name="custo' + i + '" placeholder="Ex.: 1.99" /></div>' +
                    '<div class="form-group col-md-2"><label for="total">' +
                    '<strong>Total</strong> <span class="form-requerido">*</span>' +
                    '</label><input id="total' + i + '" type="text" class="form-control" value="0"' +
                    ' name="total' + i + '" readonly="readonly" /></div><div class="form-group col-md-2">' +
                    '<label><strong>Ação</strong></label><div><button type="button" id="remover' + i + '"' +
                    'class="btn btn-danger btn-block" disabled="disabled">REMOVER</button></div></div>');
                remover = '#remover' + i;
                $(remover).attr("disabled", false);
                linha = '#linha' + i;
                quantidade = '#quantidade' + i;
                custo = '#custo' + i;
                total = '#total' + i;

                $(quantidade).keyup(function () {
                    $(this).val(this.value.replace(/\D/g, ''));
                });

                $(custo).blur(function () {
                    if ($(custo).val().split(".")) {
                        var parte = $(custo).val().split(".");
                        var valor = parte[0] + "." + parte[1];
                        totalParcial = valor * $(quantidade).val();
                        if (totalParcial > 0 || totalParcial > 0.00) {
                            $(total).val(totalParcial.toFixed(2).replace('.', '.'));
                            $(remover).attr("disabled", true);
                        } else {
                            totalParcial = 0;
                            $(total).val(0);
                            $(remover).attr("disabled", false);
                        }
                    }
                });

                $(remover).click(function (e) {
                    e.preventDefault();
                    $(linha).remove();
                });
            });

            $('#calcular').click(function (e) {
                e.preventDefault();
                console.log(totais);
                if ($(total).val() == 0 || $(total).val() == '') {
                    return
                } else {
                    $(quantidade).add(custo).prop("readonly", true);
                    totais.push(totalParcial);
                    var totalCompra = 0;
                    for (var y = 0; y < totais.length; y++) {
                        totalCompra += totais[y];
                    }
                    $('#totalItens').html(totais.length);
                    $('#totalCompra').html('R$ ' + totalCompra.toFixed(2).replace('.', ','));
                    $('#cadastrar').attr("disabled", false);
                }
            });
        });
    </script>
    @endsection