@extends('layouts.dashboard', ["current" => "admin"])
@section('conteudo')
<!-- FIM DO MENU LATERAL ESQUERDO -->
<!-- Page wrapper  -->
<div class="page-wrapper">    
    <div class="page-breadcrumb"><!-- Bread crumb and right sidebar toggle -->
        <div class="row align-items-center">
            <div class="col-5">
                <h4 class="page-title">DASHBOARD</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <!--<li class="breadcrumb-item active" aria-current="page"><a href="/admin">Home</a></li>-->
                            <!--<li class="breadcrumb-item active" aria-current="page">Library</li>-->
                            <li class="breadcrumb-item active" aria-current="page">Home</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-7">
                <div class="text-right upgrade-btn">
                    <a href="#" class="btn btn-danger text-white"><i class="fas fa-plus-square"></i> NOVO PEDIDO</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Sales chart -->
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex align-items-center">
                            <div>
                                <h4 class="card-title">PEDIDOS</h4>
                                <h5 class="card-subtitle mb-4">Exebindo 6
                                    pedido(s) de 34.</h5>
                            </div>
                            <div class="ml-auto d-flex no-block align-items-center">
                                <ul class="list-inline font-12 dl m-r-15 m-b-0">
                                    <li class="list-inline-item text-muted">03 DEZ. 2018</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <!-- column -->
                            <div class="col-lg-12 o-auto" style="height: 15.8rem">
                                <div class="table-responsive">
                                    <table class="table v-middle text-nowrap">
                                        <thead>
                                            <tr class="bg-light">
                                                <th class="border-top-0">N°</th>
                                                <th class="border-top-0">CLIENTE</th>
                                                <th class="border-top-0 text-center">CONTATO</th>
                                                <th class="border-top-0 text-center">TOTAL</th>
                                                <th class="border-top-0 text-center">SITUAÇÃO</th>
                                                <th class="border-top-0 text-center">AÇÃO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><a href="#" class="text-secondary">
                                                        <img src="/storage/imagens/Tk9RFKaubGdP1Drj91ljHezdxCQSkJfJbE0mG5yn.jpeg"
                                                            class="rounded-circle" width="40" height="40" />
                                                        &nbsp;<strong>Silvio Santos</strong></a>
                                                </td>
                                                <td class="text-center"><a href="#" class="text-primary">VER</a></td>
                                                <td class="text-center"><strong>R$ 45,00</strong></td>
                                                <td class="text-center"><label
                                                        class="label label-danger">Aguardando</label></td>
                                                <td class="text-center">
                                                    <a href="#" class="text-info" title="Editar"><i
                                                            class="mdi mdi-pencil"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td><a href="#" class="text-secondary">
                                                        <img src="/storage/imagens/mkoqCFBu9YC5YPv1R82ResBUN2aUtkcBRgPtc4lu.jpeg"
                                                            class="rounded-circle" width="40" height="40" />
                                                        &nbsp;<strong>Mara Maravilha</strong></a>
                                                </td>
                                                <td class="text-center"><a href="#" class="text-primary">VER</a></td>
                                                <td class="text-center"><strong>R$ 32,50</strong></td>
                                                <td class="text-center"><label
                                                        class="label label-success">Pronto</label></td>
                                                <td class="text-center">
                                                    <a href="#" class="text-info" title="Editar"><i
                                                            class="mdi mdi-pencil"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td><a href="#" class="text-secondary">
                                                        <img src="/storage/imagens/gyQveo89EKtbJpfu96LNBrFUTQR8G6hC2XOkzpgt.jpeg"
                                                            class="rounded-circle" width="40" height="40" />
                                                        &nbsp;<strong>Pedro Bial</strong></a>
                                                </td>
                                                <td class="text-center"><a href="#" class="text-primary">VER</a></td>
                                                <td class="text-center"><strong>R$ 19,00</strong></td>
                                                <td class="text-center"><label
                                                        class="label label-danger">Aguardando</label></td>
                                                <td class="text-center">
                                                    <a href="#" class="text-info" title="Editar"><i
                                                            class="mdi mdi-pencil"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td><a href="#" class="text-secondary">
                                                        <img src="/storage/imagens/tHRUV7aKh5hAZrz9dWW9qdPusDuiPmUW88rh1zXD.jpeg"
                                                            class="rounded-circle" width="40" height="40" />
                                                        &nbsp;<strong>Rita Cadillac</strong></a>
                                                </td>
                                                <td class="text-center"><a href="#" class="text-primary">VER</a></td>
                                                <td class="text-center"><strong>R$ 69,00</strong></td>
                                                <td class="text-center"><label
                                                        class="label label-success">Pronto</label></td>
                                                <td class="text-center">
                                                    <a href="#" class="text-info" title="Editar"><i
                                                            class="mdi mdi-pencil"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td><a href="#" class="text-secondary">
                                                        <img src="/storage/imagens/h7O1hP4WqFb8C8SoPTHmBdvufRewozA5f130ae52.jpeg"
                                                            class="rounded-circle" width="40" height="40" />
                                                        &nbsp;<strong>Tata Werneck</strong></a>
                                                </td>
                                                <td class="text-center"><a href="#" class="text-primary">VER</a></td>
                                                <td class="text-center"><strong>R$ 15,35</strong></td>
                                                <td class="text-center"><label
                                                        class="label label-danger">Aguardando</label></td>
                                                <td class="text-center">
                                                    <a href="#" class="text-info" title="Editar"><i
                                                            class="mdi mdi-pencil"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td><a href="#" class="text-secondary">
                                                        <img src="/storage/imagens/mIwlKf6w6mVPIRo1ySNVjn13cFCC90aCj13PKLqk.jpeg"
                                                            class="rounded-circle" width="40" height="40" />
                                                        &nbsp;<strong>Marcos Mion</strong></a>
                                                </td>
                                                <td class="text-center"><a href="#" class="text-primary">VER</a></td>
                                                <td class="text-center"><strong>R$ 453,00</strong></td>
                                                <td class="text-center"><label
                                                        class="label label-danger">Aguardando</label></td>
                                                <td class="text-center">
                                                    <a href="#" class="text-info" title="Editar"><i
                                                            class="mdi mdi-pencil"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- column -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">CONTROLE DE PEDIDOS</h4>
                        <div class="feed-widget">
                            <ul class="list-style-none feed-body m-0 p-b-20">
                                <li class="feed-item">
                                    <div class="feed-icon bg-info"><i class="far fa-money-bill-alt"></i></div> TOTAL EM
                                    VENDAS:
                                    <span class="ml-auto font-14"><strong>R$ 1.543,08</strong></span>
                                </li>
                                <li class="feed-item">
                                    <div class="feed-icon bg-success"><i class="ti-shopping-cart"></i></div> TOTAL
                                    EFETIVADOS:
                                    <span class="ml-auto font-14"><strong>2</strong></span>
                                </li>
                                <li class="feed-item">
                                    <div class="feed-icon bg-warning"><i class="fas fa-times"></i></div> TOTAL DE
                                    CALCELAMENTO: <span class="ml-auto font-14"><strong>0</strong></span>
                                </li>
                                <li class="feed-item">
                                    <div class="feed-icon bg-danger"><i class="far fa-clock"></i></div> TOTAL
                                    AGUARDANDO:
                                    <span class="ml-auto font-14"><strong>4</strong></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sales chart -->
        <!-- Table -->
        <div class="row">
            <!-- column -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- title -->
                        <div class="d-md-flex align-items-center">
                            <div>
                                <h4 class="card-title">FATURAMENTO</h4>
                                <h5 class="card-subtitle">Exibindo histórico da última semana.</h5>
                            </div>
                            <div class="ml-auto">
                                <div class="dl">
                                    <select class="custom-select">
                                        <option value="0" selected>FATURADO</option>
                                        <option value="1">SEMANA</option>
                                        <option value="2">QUANTIDADE</option>
                                        <option value="3">DESTAQUE</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- title -->
                    </div>
                    <div class="table-responsive">
                        <table class="table v-middle">
                            <thead>
                                <tr class="bg-light">
                                    <th class="border-top-0">QUANDO</th>
                                    <th class="border-top-0">LANCHE</th>
                                    <th class="border-top-0">DESTAQUE</th>
                                    <th class="border-top-0">SITUAÇÃO</th>
                                    <th class="border-top-0">PEDIDOS</th>
                                    <th class="border-top-0">VENDAS</th>
                                    <th class="border-top-0">FATURADO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="m-r-10"><a class="btn btn-circle btn-info text-white">TR</a>
                                            </div>
                                            <div class="">
                                                <h4 class="m-b-0 font-16">Terça-feira</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Ogro burger de Carne</td>
                                    <td>Pedro Bial</td>
                                    <td>
                                        <label class="label label-danger">Aguardando</label>
                                    </td>
                                    <td>123</td>
                                    <td>67</td>
                                    <td>
                                        <h5 class="m-b-0">R$ 2850,06</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="m-r-10"><a class="btn btn-circle btn-orange text-white">SG</a>
                                            </div>
                                            <div class="">
                                                <h4 class="m-b-0 font-16">Segunda-feira</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Ogro Vegano</td>
                                    <td>Mara Maravilha</td>
                                    <td>
                                        <label class="label label-info">Modificado</label>
                                    </td>
                                    <td>46</td>
                                    <td>22</td>
                                    <td>
                                        <h5 class="m-b-0">R$ 2009.87</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="m-r-10"><a class="btn btn-circle btn-success text-white">DO</a>
                                            </div>
                                            <div class="">
                                                <h4 class="m-b-0 font-16">Domingo</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Ogro Misto</td>
                                    <td>Marcos Mion</td>
                                    <td>
                                        <label class="label label-success">Pronto</label>
                                    </td>
                                    <td>100</td>
                                    <td>68</td>
                                    <td>
                                        <h5 class="m-b-0">R$ 1850,32</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="m-r-10"><a class="btn btn-circle btn-purple text-white">QU</a>
                                            </div>
                                            <div class="">
                                                <h4 class="m-b-0 font-16">Quinta-feira</h4>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Ogro de Tomate</td>
                                    <td>Mr. Catra</td>
                                    <td>
                                        <label class="label label-warning">Cancelado</label>
                                    </td>
                                    <td>15</td>
                                    <td>4</td>
                                    <td>
                                        <h5 class="m-b-0">R$ 1000,45</h5>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Table -->
        <!-- Recent comment and chats -->
        <div class="row">
            <!-- column -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">DESTAQUES</h4>
                    </div>
                    <div class="comment-widgets scrollable">
                        <!-- Comment Row -->
                        <div class="d-flex flex-row comment-row m-t-0">
                            <div class="p-2"><img src="/storage/imagens/tHRUV7aKh5hAZrz9dWW9qdPusDuiPmUW88rh1zXD.jpeg"
                                    alt="user" width="50" class="rounded-circle"></div>
                            <div class="comment-text w-100">
                                <h6 class="font-medium">Rita Cadillac</h6>
                                <span class="m-b-15 d-block">Lorem Ipsum is simply dummy text of the printing and type
                                    setting industry. </span>
                                <div class="comment-footer">
                                    <span class="text-muted float-right">Dezembro 14, 2018</span> <span
                                        class="text-secundary"><strong>1500 pontos</strong></span>
                                    <!--<span class="action-icons">
                                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-check"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-heart"></i></a>
                                    </span>-->
                                </div>
                            </div>
                        </div>
                        <!-- Comment Row -->
                        <div class="d-flex flex-row comment-row">
                            <div class="p-2"><img src="/storage/imagens/h7O1hP4WqFb8C8SoPTHmBdvufRewozA5f130ae52.jpeg"
                                    alt="user" width="50" class="rounded-circle"></div>
                            <div class="comment-text active w-100">
                                <h6 class="font-medium">Tata Werneck</h6>
                                <span class="m-b-15 d-block">Lorem Ipsum is simply dummy text of the printing and type
                                    setting industry. </span>
                                <div class="comment-footer ">
                                    <span class="text-muted float-right">Novembro 05, 2018</span>
                                    <span class="text-secundary"><strong>1230 pontos</strong></span>
                                    <!--<span class="action-icons active">
                                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                        <a href="javascript:void(0)"><i class="icon-close"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-heart text-danger"></i></a>
                                    </span>-->
                                </div>
                            </div>
                        </div>
                        <!-- Comment Row -->
                        <div class="d-flex flex-row comment-row">
                            <div class="p-2"><img src="/storage/imagens/gyQveo89EKtbJpfu96LNBrFUTQR8G6hC2XOkzpgt.jpeg"
                                    alt="user" width="50" class="rounded-circle"></div>
                            <div class="comment-text w-100">
                                <h6 class="font-medium">Pedro Bial</h6>
                                <span class="m-b-15 d-block">Lorem Ipsum is simply dummy text of the printing and type
                                    setting industry. </span>
                                <div class="comment-footer">
                                    <span class="text-muted float-right">Setembro 10, 2018</span>
                                    <span class="text-secundary"><strong>950 pontos</strong></span>
                                    <!--<span class="action-icons">
                                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-check"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-heart"></i></a>
                                    </span>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- column -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">PREVISÃO DO TEMPO</h4>
                        <div class="d-flex align-items-center flex-row m-t-30">
                            <div class="display-5 text-info"><i class="wi wi-day-showers"></i>
                                <span>73<sup>°</sup></span></div>
                            <div class="m-l-10">
                                <h3 class="m-b-0">Sexta-feira</h3><small>Pelotas-RS, Brasil</small>
                            </div>
                        </div>
                        <table class="table no-border mini-table m-t-20">
                            <tbody>
                                <tr>
                                    <td class="text-muted">Vento</td>
                                    <td class="font-medium">ESE 17 mph</td>
                                </tr>
                                <tr>
                                    <td class="text-muted">Humidade</td>
                                    <td class="font-medium">83%</td>
                                </tr>
                                <tr>
                                    <td class="text-muted">Pressão</td>
                                    <td class="font-medium">28.56 in</td>
                                </tr>
                                <tr>
                                    <td class="text-muted">Nuvens</td>
                                    <td class="font-medium">78%</td>
                                </tr>
                            </tbody>
                        </table>
                        <ul class="row list-style-none text-center m-t-30">
                            <li class="col-3">
                                <h4 class="text-info"><i class="wi wi-day-sunny"></i></h4>
                                <span class="d-block text-muted">09:30</span>
                                <h3 class="m-t-5">70<sup>°</sup></h3>
                            </li>
                            <li class="col-3">
                                <h4 class="text-info"><i class="wi wi-day-cloudy"></i></h4>
                                <span class="d-block text-muted">11:30</span>
                                <h3 class="m-t-5">72<sup>°</sup></h3>
                            </li>
                            <li class="col-3">
                                <h4 class="text-info"><i class="wi wi-day-hail"></i></h4>
                                <span class="d-block text-muted">13:30</span>
                                <h3 class="m-t-5">75<sup>°</sup></h3>
                            </li>
                            <li class="col-3">
                                <h4 class="text-info"><i class="wi wi-day-sprinkle"></i></h4>
                                <span class="d-block text-muted">15:30</span>
                                <h3 class="m-t-5">76<sup>°</sup></h3>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Recent comment and chats -->
    </div>
    @endsection