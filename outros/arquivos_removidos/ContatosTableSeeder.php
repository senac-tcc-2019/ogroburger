<?php

use Illuminate\Database\Seeder;

class ContatosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contatos')->insert([
            'cep' => '96030-390',
            'rua' => 'R. Mal. Setembrino de Carvalho',
            'numero' => 104,
            'complemento' => '',
            'bairro' => 'Fragata',
            'cidade' => 'Pelotas',
            'uf' => 'RS',
            'whatsapp' => '(53) 98143-1723',
            'celular' => '(53) 98445-7095',
            'residencial' => '',
            'facebook' => 'https://pt-br.facebook.com/rafael.souzacalearo',
            'twitter' => 'https://twitter.com/@rafatheonly',
            'instagram' => 'https://www.instagram.com/rafaelsouzacalearo/',
            'admin_id' => 1            
        ]);
    }
}
