<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lanches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto')->default("imagens/lanche.png");  
            $table->string('nome');            
            $table->double('preco', 10, 2)->default(0.00);          
            $table->string('categoria');          
            $table->integer('ativo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lanches');
    }
}
