<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMercadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mercados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razaosocial');
            $table->string('cnpj')->nullable()->default("—");
            $table->string('email')->nullable()->default("—");
            $table->string('telefone')->nullable()->default("—");
            $table->string('celular')->nullable()->default("—");
            $table->string('site')->nullable()->default("—");
            $table->string('foto')->default("imagens/mercado.png"); 
            $table->integer('ativo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mercados');
    }
}
