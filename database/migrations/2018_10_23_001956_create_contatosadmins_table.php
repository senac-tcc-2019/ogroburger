<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosadminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatosadmins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cep')->nullable()->default("—");
            $table->string('rua')->nullable()->default("—");
            $table->string('numero')->nullable()->default("—");
            $table->string('complemento')->nullable()->default("—");
            $table->string('bairro')->nullable()->default("—");
            $table->string('cidade')->nullable()->default("—");
            $table->string('uf')->nullable()->default("—");           
            $table->string('celular1')->nullable()->default("—");
            $table->string('celular2')->nullable()->default("—");
            $table->string('residencial')->nullable()->default("—");
            $table->string('facebook')->nullable()->default("—");
            $table->string('twitter')->nullable()->default("—");
            $table->string('instagram')->nullable()->default("—");
            $table->integer('admin_id')->unsigned();
            $table->foreign('admin_id')->references('id')->on('admins');        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatosadmins');
    }
}
