<?php

use Illuminate\Database\Seeder;

class RecheiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //LANCHE 1 = BROCOLIS
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 1,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 1,
            'lanche_id' => 1,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 4, 
            'quantidadeproduto' => 1,
            'lanche_id' => 1,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 6, 
            'quantidadeproduto' => 1,
            'lanche_id' => 1,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 8, 
            'quantidadeproduto' => 1,
            'lanche_id' => 1,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        //LANCHE 2 = OGRO BUGER
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 2,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 3, 
            'quantidadeproduto' => 1,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 12, 
            'quantidadeproduto' => 1,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 7, 
            'quantidadeproduto' => 1,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 8, 
            'quantidadeproduto' => 1,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 6, 
            'quantidadeproduto' => 1,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 9, 
            'quantidadeproduto' => 1,
            'lanche_id' => 2,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        //LANHE 3 = CARNUDAO
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 3,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 2,
            'lanche_id' => 3,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 3, 
            'quantidadeproduto' => 2,
            'lanche_id' => 3,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 12, 
            'quantidadeproduto' => 2,
            'lanche_id' => 3,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 8, 
            'quantidadeproduto' => 2,
            'lanche_id' => 3,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 11, 
            'quantidadeproduto' => 2,
            'lanche_id' => 3,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        //LANCHE 4 = FRANGO
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 4,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 1,
            'lanche_id' => 4,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 4, 
            'quantidadeproduto' => 1,
            'lanche_id' => 4,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 12, 
            'quantidadeproduto' => 1,
            'lanche_id' => 4,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 8, 
            'quantidadeproduto' => 1,
            'lanche_id' => 4,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 6, 
            'quantidadeproduto' => 1,
            'lanche_id' => 4,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 9, 
            'quantidadeproduto' => 1,
            'lanche_id' => 4,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        //LANCHE 5 = FEIJAO PICANTE
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 5,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 1,
            'lanche_id' => 5,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 3, 
            'quantidadeproduto' => 1,
            'lanche_id' => 5,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 8, 
            'quantidadeproduto' => 1,
            'lanche_id' => 5,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        //LANCHE 6 = OGRO JR
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 4, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 12, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 7, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 8, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 6, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 9, 
            'quantidadeproduto' => 1,
            'lanche_id' => 6,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        //LANCHE 7 = PRIMAVEIRA 100% VEGAN
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 7,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 1,
            'lanche_id' => 7,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 4, 
            'quantidadeproduto' => 1,
            'lanche_id' => 7,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 5, 
            'quantidadeproduto' => 1,
            'lanche_id' => 7,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 8, 
            'quantidadeproduto' => 1,
            'lanche_id' => 7,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 10, 
            'quantidadeproduto' => 1,
            'lanche_id' => 7,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        //LANCHE 8 = PROTEINA DE SOJA
        DB::table('recheios')->insert([
            'produto_id' => 1, 
            'quantidadeproduto' => 1,
            'lanche_id' => 8,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 2, 
            'quantidadeproduto' => 1,
            'lanche_id' => 8,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 3, 
            'quantidadeproduto' => 1,
            'lanche_id' => 8,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 11, 
            'quantidadeproduto' => 1,
            'lanche_id' => 8,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('recheios')->insert([
            'produto_id' => 5, 
            'quantidadeproduto' => 1,
            'lanche_id' => 8,            
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
    }
}
