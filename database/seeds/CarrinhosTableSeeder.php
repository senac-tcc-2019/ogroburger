<?php

use Illuminate\Database\Seeder;

class CarrinhosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carrinhos')->insert([
            'pedido_id' => 1, 'quantidade' => 1, 'user_id' => 4, 'lanche_id' => 1,            
            'created_at' => '2019-11-04 23:34:23', 'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('carrinhos')->insert([
            'pedido_id' => 1, 'quantidade' => 1, 'lanche_id' => 2,            
            'created_at' => '2019-11-04 23:34:23', 'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('carrinhos')->insert([
            'pedido_id' => 1, 'quantidade' => 2, 'lanche_id' => 9,            
            'created_at' => '2019-11-04 23:34:23', 'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('carrinhos')->insert([
            'pedido_id' => 2, 'quantidade' => 1, 'user_id' => 10, 'lanche_id' => 3,            
            'created_at' => '2019-11-04 23:34:23', 'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('carrinhos')->insert([
            'pedido_id' => 2, 'quantidade' => 1, 'lanche_id' => 10,            
            'created_at' => '2019-11-04 23:34:23', 'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('carrinhos')->insert([
            'pedido_id' => 3, 'quantidade' => 2, 'user_id' => 4, 'lanche_id' => 6,            
            'created_at' => '2019-11-04 23:34:23', 'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('carrinhos')->insert([
            'pedido_id' => 3, 'quantidade' => 2, 'lanche_id' => 10,            
            'created_at' => '2019-11-04 23:34:23', 'updated_at' => '2019-11-04 23:35:46'
        ]);
    }
}
