<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ITEM 1 = PAO
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Pão',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:31:25',
            'updated_at' => '2018-12-01 18:31:25'
        ]);

        //ITEM 2 = MAIONESE 
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Maionese',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:31:34',
            'updated_at' => '2018-12-01 18:31:35'
        ]);

        //ITEM 3 = HAMBURGUER DE CARNE
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Hambúrguer de Carne',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:32:40',
            'updated_at' => '2018-12-01 18:32:43'
        ]);

        //ITEM 4 = HAMBURGUER DE FRANGO
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Hambúrguer de Frango',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 5 = CEBOLA
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Cebola',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 6 = CEBOLA ROXA
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Cebola Roxa',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 7 = BACON
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Bacon',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 8 = TOMATE
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Tomate',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 9 = ALFACE AMERICANA
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Alface Americana',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 10 = ALFACE
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Alface',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 11 = RÚCULA
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Rúcula',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);

        //ITEM 12 = QUEIJO CHEDDAR
        DB::table('produtos')->insert([
            'foto' => 'imagens/produto.png',
            'nome' => 'Queijo Cheddar',             
            'ativo' => 0, 
            'created_at' => '2018-12-01 18:33:25',
            'updated_at' => '2018-12-01 18:33:26'
        ]);
    }
}
