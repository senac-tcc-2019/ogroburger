<?php

use Illuminate\Database\Seeder;

class LanchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lanches')->insert([
            'id' => 1, 
            'foto' => 'imagens/lanches/OGRO-BROCOLIS.jpg',
            'nome' => 'Ogro Brócolis',
            'preco' => 18.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        DB::table('lanches')->insert([
            'id' => 2, 
            'foto' => 'imagens/lanches/OGRO-BURGER.jpg',
            'nome' => 'Ogro Burger',
            'preco' => 19.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        DB::table('lanches')->insert([
            'id' => 3, 
            'foto' => 'imagens/lanches/OGRO-CARNUDAO.png',
            'nome' => 'Ogro Carnudão',
            'preco' => 29.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        DB::table('lanches')->insert([
            'id' => 4, 
            'foto' => 'imagens/lanches/OGRO-DE-FRANGO.jpg',
            'nome' => 'Ogro de Frango',
            'preco' => 16.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        DB::table('lanches')->insert([
            'id' => 5, 
            'foto' => 'imagens/lanches/OGRO-FEIJAO-PICANTE.jpg',
            'nome' => 'Ogro Feijão Picante',
            'preco' => 13.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        DB::table('lanches')->insert([
            'id' => 6, 
            'foto' => 'imagens/lanches/OGRO-JR.jpg',
            'nome' => 'Ogro Jr.',
            'preco' => 11.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        DB::table('lanches')->insert([
            'id' => 7, 
            'foto' => 'imagens/lanches/OGRO-PRIMAVERA-100%-VEGAN.jpg',
            'nome' => 'Ogro Primavera 100% Vegan',
            'preco' => 20.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);

        DB::table('lanches')->insert([
            'id' => 8, 
            'foto' => 'imagens/lanches/OGRO-PROTEINA-DE-SOJA.jpg',
            'nome' => 'Ogro Proteína de Soja',
            'preco' => 13.90,
            'categoria' => 'LANCHE',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('lanches')->insert([
            'id' => 9, 
            'foto' => 'imagens/lanches/SUCO-290.png',
            'nome' => 'Suco Del Valle',
            'preco' => 3.50,
            'categoria' => 'BEBIDA',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
        DB::table('lanches')->insert([
            'id' => 10, 
            'foto' => 'imagens/lanches/COCA-SEM-ACUCAR-350.png',
            'nome' => 'Coca-Cola Sem Açúcar',
            'preco' => 3.00,
            'categoria' => 'BEBIDA',
            'created_at' => '2019-10-31 23:34:23',
            'updated_at' => '2019-10-31 23:35:46'
        ]);
    }
}
