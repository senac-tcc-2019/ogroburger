<?php

use Illuminate\Database\Seeder;

class PedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedidos')->insert([
            'id' => 1,             
           // 'user_id' => 3,
            'total' => 45.80,
            'status' => 'AGUARDANDO',            
            'created_at' => '2019-11-04 19:10:23',
            'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('pedidos')->insert([
            'id' => 2,             
            //'user_id' => 4,
            'total' => 32.90,
            'status' => 'AGUARDANDO',
            'created_at' => '2019-11-04 19:15:23',
            'updated_at' => '2019-11-04 23:35:46'
        ]);
        DB::table('pedidos')->insert([
            'id' => 3,             
            //'user_id' => 3,
            'total' => 29.80,
            'status' => 'AGUARDANDO',
            'created_at' => '2019-11-04 19:55:23',
            'updated_at' => '2019-11-04 23:35:46'
        ]);
    }
}
