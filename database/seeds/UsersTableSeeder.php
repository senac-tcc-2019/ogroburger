<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Silvio Santos',
            'email' => 'silviosantos@sbt.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/Tk9RFKaubGdP1Drj91ljHezdxCQSkJfJbE0mG5yn.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-01 01:10:25',
            'updated_at' => '2018-11-01 01:11:53',
        ]);
        DB::table('users')->insert([
            'name' => 'Pedro Bial',
            'email' => 'pedro.bial@globo.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/gyQveo89EKtbJpfu96LNBrFUTQR8G6hC2XOkzpgt.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 00:10:25',
            'updated_at' => '2018-11-02 00:11:53',
        ]);
        DB::table('users')->insert([
            'name' => 'Rita Cadillac',
            'email' => 'ritinha69@gmail.com',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/tHRUV7aKh5hAZrz9dWW9qdPusDuiPmUW88rh1zXD.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 00:30:18',
            'updated_at' => '2018-11-02 00:45:50',
        ]);
        DB::table('users')->insert([
            'name' => 'Tata Werneck',
            'email' => 'tata_werneck@hotmail.com',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/h7O1hP4WqFb8C8SoPTHmBdvufRewozA5f130ae52.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 15:23:32',
            'updated_at' => '2018-11-02 15:32:16',
        ]);
        DB::table('users')->insert([
            'name' => 'Marcos Mion',
            'email' => 'marcos-mion@rederecord.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/mIwlKf6w6mVPIRo1ySNVjn13cFCC90aCj13PKLqk.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 18:51:31',
            'updated_at' => '2018-11-02 19:04:01',
        ]);
        DB::table('users')->insert([
            'name' => 'Rafinha Bastos',
            'email' => 'rafinha.bastos@gmail.com',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/c7tza9JZ5N1Zbzj5NArNDNQwS4mD27htfFAz2BIX.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 19:52:05',
            'updated_at' => '2018-11-02 20:18:22',
        ]);
        DB::table('users')->insert([
            'name' => 'Moacyr Franco',
            'email' => 'moacyr.f@yahoo.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/oILVKa40lff5SAjMaky3yAaZMH2CbUqDLRZJLk8A.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 20:27:05',
            'updated_at' => '2018-11-02 20:29:06',
        ]);
        DB::table('users')->insert([
            'name' => 'Renata Fan',
            'email' => 'renatinhafan@bandtv.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/9fF4JdWASwxGQGO6QPomuP62SccLzR2kOrfq2nnm.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 21:26:46',
            'updated_at' => '2018-11-02 21:29:41',
        ]);
        DB::table('users')->insert([
            'name' => 'Mara Maravilha',
            'email' => 'maramaravilha@bol.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/mkoqCFBu9YC5YPv1R82ResBUN2aUtkcBRgPtc4lu.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 21:34:30',
            'updated_at' => '2018-11-02 21:36:05',
        ]);
        DB::table('users')->insert([
            'name' => 'Sidney Magal',
            'email' => 'sidney-magal@hotmail.com',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/Y1VsF85QMo7lq94Fyl2o70Bc1tMeQhpMzj4EUpxK.jpeg',
            'ativo' => 0, 'created_at' => '2018-11-02 22:03:49',
            'updated_at' => '2018-11-02 22:05:11',
        ]);
        DB::table('users')->insert([
            'name' => 'Dercy Gonçalves',
            'email' => 'putaquepariu@hotmail.com',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/GGhzgbsX5tgSJo5Zp8GAxTBqhpevatGSopL9kEyO.jpeg',
            'ativo' => 1, 'created_at' => '2018-11-08 20:51:49',
            'updated_at' => '2018-11-08 20:51:49',
        ]);
        DB::table('users')->insert([
            'name' => 'Hebe Camargo',
            'email' => 'hebecamargo@sbt.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/OKxLFD32vbkNQDnyZXlmLPchFdixJ09Lyk821TPb.jpeg',
            'ativo' => 1, 'created_at' => '2018-11-10 11:27:49',
            'updated_at' => '2018-11-10 11:27:49',
        ]);
        DB::table('users')->insert([
            'name' => 'Raul Seixas',
            'email' => 'tocaraul@gmail.com',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/JPlXQgJw0Mgj8daI7JuAyzO40am4l1QyUShgBhQA.jpeg',
            'ativo' => 1, 'created_at' => '2018-11-10 11:40:49',
            'updated_at' => '2018-11-10 11:40:49',
        ]);
        DB::table('users')->insert([
            'name' => 'Pablo Escobar',
            'email' => 'escobar@outlook.com',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/D1mRicoiJxqK7SmRz3KuafYdSKR9OZMkTlRdlfPZ.jpeg',
            'ativo' => 1, 'created_at' => '2018-11-10 11:57:49',
            'updated_at' => '2018-11-10 11:57:49',
        ]);
        DB::table('users')->insert([
            'name' => 'Mr. Catra',
            'email' => 'paidetodos@yahoo.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/Xynq9AdmdlM64hhxAuU24FlTgbIaDvtDKMzq7Opc.jpeg',
            'ativo' => 1, 'created_at' => '2018-11-10 12:00:49',
            'updated_at' => '2018-11-10 12:00:49',
        ]);
        DB::table('users')->insert([
            'name' => 'Danilo Gentili',
            'email' => 'danilogentili@sbt.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/perfil.png',
            'ativo' => 0, 'created_at' => '2018-12-02 17:07:02',
            'updated_at' => '2018-12-02 17:07:02',
        ]);
        DB::table('users')->insert([
            'name' => 'Tarcísio Meira',
            'email' => 'tarcisio-meira@globo.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/perfil.png',
            'ativo' => 0, 'created_at' => '2018-12-02 17:13:50',
            'updated_at' => '2018-12-02 17:13:50',
        ]);
        DB::table('users')->insert([
            'name' => 'Fausto Silva',
            'email' => 'olocobicho@globo.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/perfil.png',
            'ativo' => 1, 'created_at' => '2018-12-02 15:22:50',
            'updated_at' => '2018-12-02 15:22:50',
        ]);
        DB::table('users')->insert([
            'name' => 'Lívia Andrade',
            'email' => 'livia_andrade@sbt.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/perfil.png',
            'ativo' => 0, 'created_at' => '2018-12-02 15:24:50',
            'updated_at' => '2018-12-02 15:24:50',
        ]);
        DB::table('users')->insert([
            'name' => 'Rafael Portugual',
            'email' => 'rafaportuga@bol.com.br',
            'password' => bcrypt('123456'), 'perfil' => 'CLIENTE',
            'foto' => 'imagens/perfil.png',
            'ativo' => 0, 'created_at' => '2018-12-02 15:26:50',
            'updated_at' => '2018-12-02 15:26:50',
        ]);
    }
}
