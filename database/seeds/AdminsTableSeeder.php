<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Rafael Calearo',
            'email' => 'rafaelcalearo@hotmail.com',
            'password' => bcrypt('123456'),
            'perfil' => 'ADMIN',
            'foto' => 'imagens/D8gf3UHk58wObdW5a0giprmrm0qixbjcFcZdweqx.jpeg',
            'ativo' => 0
        ]);
    }
}
